import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import { RootSiblingParent } from 'react-native-root-siblings';
import * as Linking from 'expo-linking';
import Constants from 'expo-constants';
import * as Segment from 'expo-analytics-segment';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { auth } from './src/configs/firebase';
import { BottomSheetModalProvider } from '@gorhom/bottom-sheet';

import TabBar from './src/components/TabBar';

import Onboarding from './src/screens/Onboarding';
import Home from './src/screens/Home';
import Calendar from './src/screens/Calendar';
import Event from './src/screens/Event';
import Account from './src/screens/Account';
import Authentication from './src/screens/Authentication';
import Search from './src/screens/Search';
import Vendors from './src/screens/Vendors';
import Vendor from './src/screens/Vendor';

const prefix = Linking.createURL('/');
const Tab = createBottomTabNavigator();
const MainStack = createStackNavigator();
const RootStack = createStackNavigator();

Segment.initialize({
  //androidWriteKey: "",
  iosWriteKey: "XXXzpKsjZiu4Iw65SoalU51JmCuCYuGS",
});

// transition config for navigation stack
const fadeConfig = ({current}:any) => {
  return {
      cardStyle: {
          opacity: current.progress,
      },
  }
};

const TabNavigator = () => {
  return (
    <Tab.Navigator 
      tabBar={props => <TabBar {...props} />}
      //lazy={false}
      screenOptions={{}}
      >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Events" component={Calendar} />
      <Tab.Screen name="Vendors" component={Vendors} />
      <Tab.Screen name="Settings" component={Account} />
    </Tab.Navigator>
  );
}

const App = () => {

  // states
  const [ data, setData ] = useState<any>(null);
  const [ firstTimeUser, setFirstTimeUser ] = useState<any>(false);

  // TODO -> can this be moved outside of function?
  const linking = {
    prefixes: [prefix],
    config: {
      screens:{
        Onboarding:"Onboarding",
        Home:"Home",
        Search: "Search",
        Event: "Event",
        Settings: "Settings"
      }
    }
  };

  // callbacks
  const handleDeepLink = (event:any) => {
    let data = Linking.parse(event.url)
    setData(data);
  };

  // hooks
  useEffect(() => {
    const user = auth.currentUser;
    if (user) {
      console.log("Anonymous: " + user.isAnonymous + ", user ID: " + user.uid)
      // User is signed in, see docs for a list of available properties
      // https://firebase.google.com/docs/reference/js/firebase.User
      // ...
    } else {
      auth.signInAnonymously()
      .then((anonymousCredentials) => {
        console.log(auth.currentUser?.uid);
        setFirstTimeUser(anonymousCredentials.additionalUserInfo?.isNewUser);

      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        // ...
      });
    }
    return () => { }
  }, []);

  useEffect(() => {
    Segment.track("New session");
    const getInitialUrl = async () => {
      const initialUrl = await Linking.getInitialURL();
      if (initialUrl) setData(Linking.parse(initialUrl));
    }
    Linking.addEventListener("url", handleDeepLink);
    if (!data) {
      getInitialUrl();
    }
    return () => {
      Linking.removeEventListener("url", handleDeepLink);
    }
  }, []);

  //const OnboardingScreen = () => <Onboarding firstTimeUser={firstTimeUser} />

  return (
    <RootSiblingParent>
      <View style={[StyleSheet.absoluteFill, {backgroundColor: '#CE1733'}]}>
        <BottomSheetModalProvider>
        <NavigationContainer linking={linking}>
          <RootStack.Navigator 
            mode="modal" 
            headerMode="none"
            initialRouteName={"Onboarding"}
            >
            <RootStack.Screen name="Onboarding" component={Onboarding} options={{ cardStyleInterpolator: fadeConfig, gestureEnabled: false }} />
            <RootStack.Screen name="TabNavigator" component={TabNavigator} options={{ cardStyleInterpolator: fadeConfig, gestureEnabled: false }} />
            <RootStack.Screen name="Event" component={Event} options={{ gestureEnabled: true, ...TransitionPresets.ModalSlideFromBottomIOS }} />
            <RootStack.Screen name="Vendor" component={Vendor} options={{ gestureEnabled: true, ...TransitionPresets.ModalSlideFromBottomIOS }} />
            <RootStack.Screen name="Authentication" component={Authentication} options={{cardOverlayEnabled: true}} />
          </RootStack.Navigator>
        </NavigationContainer>
        </BottomSheetModalProvider>
      </View>
    </RootSiblingParent>
  );
}

export default App;