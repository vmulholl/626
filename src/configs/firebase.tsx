import firebase from 'firebase/app'

// Optionally import the services that you want to use
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";
import "firebase/functions";
import "firebase/storage";

// Initialize Firebase
export const firebaseConfig = {
    apiKey: "AIzaSyAaXoso1ZQkeMtx7f_QBGoTm91L8MkFPpk",
    authDomain: "loop-41ede.firebaseapp.com",
    projectId: "loop-41ede",
    databaseURL: "https://loop-41ede.firebaseio.com",
    storageBucket: "loop-41ede.appspot.com",
    messagingSenderId: "1067443472700",
    appId: "1:1067443472700:web:40069bbfe5d88f7fe0b639",
    measurementId: "G-TW7JJV4PEB"
};

if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
};

export const f = firebase;
export const db = firebase.firestore();
export const auth = firebase.auth();
export const storage = firebase.storage();