import colors from "./colors";

export default {
    heading: {
        small: {
            fontFamily: "Monument",
            fontSize: 17,
            letterSpacing: 15 * 0.0,
            lineHeight: 15 * 1.35,
            color: colors.black
        },
        regular: {
            fontFamily: "Monument",
            fontSize: 20,
            letterSpacing: 20 * 0.0,
            lineHeight: 20 * 1.35,
            color: colors.black
        },
        medium: {
            fontFamily: "Monument",
            fontSize: 24,
            letterSpacing: 24 * 0.0,
            lineHeight: 24 * 1.35,
            color: colors.black
        },
        large: {
            fontFamily: "Monument",
            fontSize: 28,
            letterSpacing: 28 * 0.0,
            lineHeight: 28 * 1.35,
            color: colors.black
        },
        xlarge: {
            fontFamily: "Monument",
            fontSize: 32,
            letterSpacing: 32 * 0.0,
            lineHeight: 32 * 1.35,
            color: colors.black
        },
    },
    label: {
        small: {
            //fontFamily: "Rounded-Semibold",
            textTransform: 'uppercase',
            fontWeight: '600',
            fontSize: 13,
            letterSpacing: 13 * 0.04,
            lineHeight: 13 * 1.10,
            color: colors.black
        },
        regular: {
            //fontFamily: "Rounded-Medium",
            fontWeight: '500',
            fontSize: 16,
            letterSpacing: 16 * 0.02,
            lineHeight: 16 * 1.2,
            color: colors.black
        },
        medium: {
            //fontFamily: "Rounded-Medium",
            fontWeight: '500',
            fontSize: 18,
            letterSpacing: 18 * 0.02,
            lineHeight: 18 * 1.35,
            color: colors.black
        },
        large: {
            //fontFamily: "Rounded-Semibold",
            fontWeight: '600',
            fontSize: 22,
            letterSpacing: 22 * 0.0,
            lineHeight: 22 * 1.35,
            color: colors.black
        },
    },
    paragraph: {
        small: {
            //fontFamily: "Rounded-Regular",
            fontWeight: '400',
            fontSize: 17,
            letterSpacing: 17 * 0.0,
            lineHeight: 17 * 1.40,
            color: colors.black
        },
        regular: {
            //fontFamily: "Rounded-Regular",
            fontWeight: '400',
            fontSize: 17,
            letterSpacing: 17 * 0.0,
            lineHeight: 17 * 1.40,
            color: colors.black
        },
        medium: {
            //fontFamily: "Rounded-Regular",
            fontWeight: '400',
            fontSize: 18,
            letterSpacing: 18 * 0.0,
            lineHeight: 18 * 1.35,
            color: colors.black
        },
        large: {
            //fontFamily: "Rounded-Medium",
            fontWeight: '500',
            fontSize: 19,
            letterSpacing: 19 * 0.0,
            lineHeight: 19 * 1.35,
            color: colors.black
        },
    }
};