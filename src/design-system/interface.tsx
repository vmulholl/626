import React, { useState, useEffect, useRef } from 'react';
import { View, Text, Pressable, Image, Animated } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import { MotiView } from 'moti'
import { BlurView } from 'expo-blur';
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import { Feather, Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import colors from './colors'
import dimensions from './dimensions';
import text from './text';

const CustomIcon = createIconSetFromIcoMoon(
  require('../../assets/fonts/selection.json'),
  'IcoMoon',
  'icomoon.ttf'
);

export const TopGradient = ({...props}) => {
    return (
    <View pointerEvents='none' style={{position: 'absolute', top: 0, height: 119, width: '100%'}}>
        <LinearGradient
            colors={[
              `rgba(${props.rgb || '237, 237, 234'}, 1)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.99)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.96)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.92)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.85)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.77)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.67)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.56)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.44)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.33)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.23)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.15)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.08)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.04)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.01)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0)`
          ]}
            style={{height: '100%', width: '100%'}}
        />
    </View>
    )
};

export const BottomGradient = ({...props}) => {
    return (
    <View pointerEvents='none' style={{position: 'absolute', zIndex: -1, bottom: 0, height: '100%', width: dimensions.WIDTH}}>
        <LinearGradient
            colors={[
              `rgba(${props.rgb || '237, 237, 234'}, 0)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.01)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.04)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.08)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.15)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.23)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.33)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.44)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.56)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.67)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.77)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.85)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.92)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.96)`,
              `rgba(${props.rgb || '237, 237, 234'}, 0.99)`,
              `rgba(${props.rgb || '237, 237, 234'}, 1)`
          ]}
            style={{height: '100%', width: '100%'}}
        />
    </View>
    )
};

export const Icon = ({...props}) => {
  
  return(
    <View 
        style={{height: 24, width: 24, justifyContent: 'center', alignItems: 'center'}}
        //onPress={props.onPress || null}
        >
        <CustomIcon name={props.name || 'ticket'} size={props.size || 20} color={props.color || colors.black} />
    </View>
  );
};

export const Key = ({...props}) => {

    return (
      <Pressable 
        hitSlop={24} 
        onPress={props.onPress} 
        style={{position: props.leading || props.trailing ? 'absolute' : null, left: props.leading ? 12 : null, right: props.trailing ? 12 : null, height: 32, width: !props.label ? 32 : null}}
      >
          <BlurView 
            intensity={props.background ? 100 : 0} 
            style={{flexDirection: 'row', height: '100%', paddingHorizontal: props.label ? 12 : null, justifyContent: 'center', alignItems: 'center', borderRadius: 20, overflow: 'hidden', backgroundColor: 'white'}}
          >
            { props.icon && 
              <Feather name={props.icon} size={props.size ? props.size : 20} style={{left: 0.5, color: colors.black}}/>
            }
            { props.label && 
              <Text style={[text.label.regular, {color: props.labelColor || colors.black}]}>{props.label}</Text>
            }
          </BlurView> 
      </Pressable>
    );
  }

export const FAB = () => {
    const { navigate } = useNavigation();
    return (
        <Pressable 
            style={{
                position: 'absolute',
                bottom: 40,
                height: 56,
                width: 56,
                borderRadius: 28,
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: colors.accentColor,
                shadowColor: colors.accentColor,
                shadowOffset: {
                  width: 0,
                  height: 15,
                },
                shadowOpacity: 0.3,
                shadowRadius: 15
            }} 
            onPress={() => navigate('Search')}
        >
          <Feather name='search' size={24} color={colors.softWhite} />
        </Pressable>
      );
};

export const ActionBar = ({...props}) => {
  
  const { navigate } = useNavigation();
  const [ toggled, setToggled ] = useState(false);
  
  return (
    <View style={{width: dimensions.WIDTH - 2 * 20, alignSelf: 'center', justifyContent: 'space-between', flexDirection: 'row'}}>
      {
        props.items.map((item:any, index:any) => {
          return (
            <Pressable key={index} style={{height: 48, width: (dimensions.WIDTH - 2 * 20 - 12) / props.items.length, justifyContent: 'center', borderRadius: 16, alignItems: 'center', flexDirection: 'row', backgroundColor: !toggled ? item.bgColor : item.toggledBgColor || colors.softWhite}} onPress={item.toggle ? () => setToggled(!toggled) : item.onPress}>
              <CustomIcon name={!toggled ? item.icon : item.toggledIcon || 'ticket'} size={18} color={!toggled ? item.iconColor : item.toggledIconColor || colors.black} style={{}} />
              <Text style={[text.label.regular, {paddingHorizontal: 12, color: !toggled ? item.labelColor : item.toggledColor}]}>{item.label}</Text>
            </Pressable>
          );
        })
      }
    </View>
  );
};

export const SectionTitle = ({...props}) => (
  <View style={{paddingHorizontal: 24, paddingTop: 24, paddingBottom: 8}}>
      <Text style={[text.heading.large, {maxWidth: dimensions.WIDTH * 0.75}]}>{props.title}</Text>
  </View>
);


export const ListTitle = ({...props}) => (
  <View style={{width: dimensions.WIDTH, paddingLeft: 24, paddingRight: 12, flexDirection: 'row', paddingTop: 24, justifyContent: 'space-between', paddingBottom: 8, alignItems: 'center'}}>
      <View>
          {props.overhead && <Text style={[text.label.small, {color: colors.darkGrey}]}>{props.overhead}</Text> }
          <Text style={text.label.large}>{props.title}</Text>
      </View>
      {props.more &&
          <Key label="see all" background onPress={props.onPress} />
      }
  </View>
);

export const ListLabel = ({...props}) => (
  <View style={{width: dimensions.WIDTH, paddingTop: 24, paddingBottom: 8, paddingHorizontal: 24}}>
      <Text style={[text.label.small, {color: colors.darkGrey}]}>{props.label}</Text>
  </View>
);

export const Label = ({...props}) => (
  <View style={{width: dimensions.WIDTH, paddingTop: 24, paddingBottom: 8, paddingHorizontal: 24}}>
      <Text style={[text.label.small, {color: colors.darkGrey}]}>{props.label}</Text>
  </View>
);

export const InputLabel = ({...props}) => (
  <View style={{height: 40, width: dimensions.WIDTH, paddingHorizontal: 32}}>
      <Text style={[text.label.small, {top: 18}]}>{props.label}</Text>
  </View>
);