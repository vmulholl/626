import { Dimensions } from 'react-native';
import Constants from 'expo-constants';

const HEIGHT = Math.round(Dimensions.get('window').height);
const WIDTH = Math.round(Dimensions.get('window').width);
const RATIO = HEIGHT / WIDTH;
const HEADER_SAFE_AREA = Constants.statusBarHeight;
const HEADER_HEIGHT = 56;
const HEADER_HEIGHT_AND_SAFE_AREA = HEADER_HEIGHT + HEADER_SAFE_AREA;
const FOOTER_SAFE_AREA = (RATIO <= 2) ? 16 : 40;
const MARGIN_8 = 8;
const MARGIN_12 = 12;
const MARGIN_16 = 16;
const MARGIN_24 = 24;

export default {
    HEIGHT: HEIGHT,
    WIDTH: WIDTH,
    RATIO: RATIO,
    HEADER_SAFE_AREA: HEADER_SAFE_AREA,
    HEADER_HEIGHT: HEADER_HEIGHT,
    HEADER_HEIGHT_AND_SAFE_AREA: HEADER_HEIGHT_AND_SAFE_AREA,
    FOOTER_SAFE_AREA: FOOTER_SAFE_AREA,
    MARGIN_8: MARGIN_8,
    MARGIN_12: MARGIN_12,
    MARGIN_16: MARGIN_16,
    MARGIN_24: MARGIN_24,
};