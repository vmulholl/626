import React, { useState, useEffect, useMemo } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Toast from 'react-native-root-toast';
import { auth } from '../configs/firebase';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useSharedValue } from 'react-native-reanimated';
import { StatusBar } from 'expo-status-bar';

import Header from '../components/Header';
import Wall from '../components/Wall';
import Event from './Event';

import colors from '../design-system/colors';
import text from '../design-system/text';
import dimensions from '../design-system/dimensions';


const CalendarStack = createStackNavigator();

const user = auth.currentUser;

const Account = ({...props}) => {
  
  /* let toast = Toast.show(JSON.stringify(user?.isAnonymous), {
    duration: Toast.durations.LONG,
  }); */

  /* useEffect(() => {

  }, []); */

  // navigation
  const route = useRoute();
  const { navigate } = useNavigation();

  // states
  const [ viewHeight, setViewHeight ] = useState<any>(0);
  const [ dismiss, setDismiss ] = useState<any>(false);

  // variables
  const y = useSharedValue(0);

  const CoverCopy = () => {
    return <View style={{minHeight: dimensions.HEADER_HEIGHT_AND_SAFE_AREA}} onLayout={(event) => { setViewHeight(Math.ceil(event.nativeEvent.layout.height))}}/>
  };

  const wall = [
    {
      title: "Settings",
      data:[
        //account details
        {
          id: "00",
          format: "account-details",

        }
      ]
    }
  ]; 

  const Index = ({...props}) => {
    return (
        <View style={[StyleSheet.absoluteFill, {backgroundColor: colors.pulp}]}>
          <StatusBar style="auto" />
          {<Wall
            background
            initialPosition = {dimensions.HEIGHT * 1}
            animateOnMount={true}
            cover = {{
              format: null, // map || video || image || showcase || stickers || profile-picture
              url: false,
              copyBlockHeight: viewHeight
            }}
            footer={false}
            feed={wall}
            details={<CoverCopy />}
            {...{y}}
          />}
          <Header
            // trailing="edit-profile"
            {...{y}}
          />
          {/* <FAB /> */}
        </View>
    );
  };

  return (
    <NavigationContainer independent >
      <CalendarStack.Navigator headerMode="none">
        <CalendarStack.Screen name="Index" component={Index} />
        <CalendarStack.Screen name="Event" component={Event} />
        {/* <CalendarStack.Screen name="Login" component={Login} /> */}
      </CalendarStack.Navigator>
    </NavigationContainer>
  );
  
};

export default Account;