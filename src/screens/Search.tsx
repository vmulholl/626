import React, { useState, useEffect, useMemo } from 'react';
import { StyleSheet, Text, View } from 'react-native';

// algolia
import algoliasearch from 'algoliasearch';
import { InstantSearch, Configure } from 'react-instantsearch-native';
import Toast from 'react-native-root-toast';
import { useNavigation, useRoute } from '@react-navigation/native';
import Animated, { useSharedValue } from 'react-native-reanimated';
import { StatusBar } from 'expo-status-bar';

import Header from '../components/Header';
import Wall from '../components/Wall';

import colors from '../design-system/colors';
import text from '../design-system/text';
import dimensions from '../design-system/dimensions';

const Search = ({...props}) => {

  /* let toast = Toast.show('Request failed to send.', {
    duration: Toast.durations.LONG,
  }); */

  // navigation
  const route = useRoute();
  const { navigate } = useNavigation();

  // variables
  const y = useSharedValue(0);

  // callbacks
  /* useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        // User is signed in.
        //fetchUserDocument(user.uid);
        navigate('Home')
      } else {
        // No user is signed in.
        //setCurrentUser(null);
        navigate('Home')
      }
    });
    return () => { }
  }, []); */

  return (
    <View style={[StyleSheet.absoluteFill, {backgroundColor: colors.pulp}]}>
      <StatusBar style="auto" />
      {<Wall
        cover = {{
          cover_url: true,
          thumbnail_url: null,
          thumbnail_format: null,
          initialPosition: dimensions.HEIGHT * 1
        }}
        //feed={wall}
        //details={<Intro />}
        {...{y}}
      />}
      <Header
        trailing = "account"
        {...{y}}
      />
    </View>
  );
}

export default Search;