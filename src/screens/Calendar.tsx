import React, { useState, useEffect, useMemo } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Toast from 'react-native-root-toast';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { useNavigation, useRoute } from '@react-navigation/native';
import Animated, { useSharedValue } from 'react-native-reanimated';
import { StatusBar } from 'expo-status-bar';

import Header from '../components/Header';
import Wall from '../components/Wall';

import colors from '../design-system/colors';
import text from '../design-system/text';
import dimensions from '../design-system/dimensions';
import { BottomGradient } from '../design-system/interface';
import Event from './Event';

const CalendarStack = createStackNavigator();

const Calendar = ({...props}) => {

  /* let toast = Toast.show('Request failed to send.', {
    duration: Toast.durations.LONG,
  }); */

  // navigation
  const route = useRoute();
  const { navigate } = useNavigation();

  // states
  const [ viewHeight, setViewHeight ] = useState<any>(0);

  // variables
  const y = useSharedValue(0);

  const CoverCopy = () => {
    return <View style={{minHeight: dimensions.HEADER_HEIGHT_AND_SAFE_AREA}} onLayout={(event) => { setViewHeight(Math.ceil(event.nativeEvent.layout.height))}}/>
  };

  const wall = [
    {
      title: "Calendar",
      data:[
        //tile-A
        {
          id: "a",
          format: "list",
          style: "square-thumbnail-cell",
          title: null,
          more: true,
          thumbnails: [
            {
              id: "a1",
              title: "626 Night Market",
              venue: "Santa Anita Park",
              venue_address: "285 W Huntington Dr, Arcadia, CA 91007, USA",
              label: "626 Night Market: Santa Anita Park",
              date: "July 9 - 11",
              start_month: "JUL",
              start_day: "9",
              end_day: "11",
              tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
              image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
              destination: "Event"
            },
            {
              id: "a2",
              title: "626 Night Market",
              venue: "Santa Anita Park",
              venue_address: "285 W Huntington Dr, Arcadia, CA 91007, USA",
              label: "626 Night Market: Santa Anita Park",
              date: "July 16 - 18",
              start_month: "JUL",
              start_day: "16",
              end_day: "18",
              tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
              image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
              destination: "Event"
            },
            {
              id: "a3",
              title: "626 Night Market",
              venue: "Alameda Fairgrounds",
              venue_address: "Pleasanton, California 94566, USA",
              label: "626 Night Market: Alameda Fairgrounds",
              date: "August 6 - 8",
              start_month: "AUG",
              start_day: "6",
              end_day: "8",
              tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
              image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',                
              destination: "Event"
            },
            {
              id: "a4",
              title: "626 Night Market",
              venue: "Alameda Fairgrounds",
              venue_address: "Pleasanton, California 94566, USA",
              label: "626 Night Market: Alameda Fairgrounds",
              date: "August 20 - 22",
              start_month: "AUG",
              start_day: "20",
              end_day: "22",
              tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
              image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',                
              destination: "Event"
            },
            {
              id: "a5",
              title: "626 Night Market",
              venue: "Alameda Fairgrounds",
              venue_address: "Pleasanton, California 94566, USA",
              label: "626 Night Market: Alameda Fairgrounds",
              date: "August 27 - 29",
              start_month: "AUG",
              start_day: "27",
              end_day: "29",
              tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
              image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
              destination: "Event"
            },
            {
              id: "a6",
              title: "626 Night Market",
              venue: "Santa Anita Park",
              venue_address: "285 W Huntington Dr, Arcadia, CA 91007, USA",
              label: "626 Night Market: Santa Anita Park",
              date: "September 3 - 5",
              start_month: "SEP",
              start_day: "3",
              end_day: "5",
              tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
              location: "Santa Anita Park",
              image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
              destination: "Event"
            },
            {
              id: "a7",
              title: "626 Night Market",
              venue: "Alameda Fairgrounds",
              venue_address: "Pleasanton, California 94566, USA",
              label: "626 Night Market: Alameda Fairgrounds",
              date: "September 24 - 26",
              start_month: "SEP",
              start_day: "24",
              end_day: "26",
              tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
              image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
              destination: "Event"
            }
          ]
        }
      ]
    }
  ];

  return (
    <View style={[StyleSheet.absoluteFill, {backgroundColor: colors.pulp}]}>
      <StatusBar style="auto" />
      {<Wall
        background
        initialPosition = {dimensions.HEIGHT * 1}
        animateOnMount={false}
        cover = {{
          format: null, // map || video || image || showcase
          url: false,
          //thumbnail: null,
          copyBlockHeight: viewHeight
        }}
        details={<CoverCopy />}
        feed={wall}
        footer
        {...{y}}
      />}
      <Header
        {...{y}}
      />
      {/* <FAB /> */}
    </View>
  );

};

export default Calendar;