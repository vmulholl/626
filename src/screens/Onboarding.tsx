import React, { useCallback, useRef, useMemo, useEffect, useState } from 'react';
import { StyleSheet, View, Image } from 'react-native';

import { setStatusBarHidden, StatusBar } from 'expo-status-bar';
import { useNavigation, useRoute } from '@react-navigation/native';
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import { useFonts } from 'expo-font';

const Icon = createIconSetFromIcoMoon(
    require('../../assets/fonts/selection.json'),
    'IcoMoon',
    'icomoon.ttf'
  );

const Onboarding = ({...props}) => {

    // navigation
    const route = useRoute();
    const { navigate } = useNavigation();

    // hooks
    let [fontsLoaded] = useFonts({
        IcoMoon: require('../../assets/fonts/icomoon.ttf'),
        'Monument': require('../../assets/fonts/MonumentExtended-Black.ttf')
    });

    useEffect(() => {
        setStatusBarHidden(true, 'none')
        if (fontsLoaded) {
            setTimeout(() => {navigate('TabNavigator')}, 4000);
        }
    },[fontsLoaded]);

    return (
        <View style={[StyleSheet.absoluteFill, {justifyContent: 'center', alignItems: 'center', backgroundColor: '#CE1733'}]}>
            <StatusBar style={'auto'} />
            <Image
              style={{height: '100%', width: '100%'}}
              source={require('../../assets/splash.png')}
              resizeMode='contain'            
            />
        </View>
    );
};

export default Onboarding;