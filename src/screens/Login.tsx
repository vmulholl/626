import React, { useState, useRef } from 'react';
import { StyleSheet, Text, View , TextInput} from 'react-native';

import { f, db, auth } from '../configs/firebase';
import { useNavigation } from '@react-navigation/native';
import Animated, { useSharedValue } from 'react-native-reanimated';

// Components
import Header from '../components/Header';
import { ScrollView } from 'react-native-gesture-handler';

// Design System
import text from '../design-system/text';
import colors from '../design-system/colors';
import dimensions from '../design-system/dimensions';
import { ActionBar, InputLabel, ListTitle, Icon } from '../design-system/interface';

const Login = () => {

  // navigation
  const { navigate, goBack } = useNavigation();

  // refs
  const emailInputRef = useRef<any>(null);
  const passwordInputRef = useRef<any>(null);

  // variables
  const y = useSharedValue(0);

  // state
  const [ email, setEmail ] = useState<any>();
  const [ password, setPassword ] = useState<any>();
  const [ passwordVisible, setPasswordVisible ] = useState<any>(false);

  const login = (email:string, password:string) => {
    f.auth().signInWithEmailAndPassword(email, password)
    .then((userCredential) => {
      // Signed in
      var user = userCredential.user;
      // ...
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
    })
  };

  return (
  <View style={StyleSheet.absoluteFill}>
        <ScrollView
        keyboardShouldPersistTaps='always'
          >
          <View style={{top: dimensions.HEADER_HEIGHT + 38}}>
            <ListTitle title='Welcome back!'/>
            <InputLabel label='Email' />
            <View style={{height: 48, width: dimensions.WIDTH - 32, borderRadius: 16, alignSelf: 'center', justifyContent: 'center', paddingLeft: 16, paddingRight: 8, borderWidth: 1.5, borderColor: colors.lightGrey}}>
              <TextInput
                ref={emailInputRef}
                style={[text.label.medium, {height: '100%'}]}
                autoFocus={true}
                autoCompleteType='email'
                keyboardType='email-address'
                textContentType='emailAddress'
                placeholder="you@email.com"
                clearButtonMode='while-editing'
                value={email}
                returnKeyType={'next'}
                onChangeText={(email: string) => setEmail(email)}
                onSubmitEditing={() => {passwordInputRef?.current.focus()}}
                blurOnSubmit={false}
              />
            </View>
            <InputLabel label='Password' />
            <View style={{height: 48, flexDirection: 'row', marginBottom: 24, alignItems: 'center', width: dimensions.WIDTH - 32, borderRadius: 16, alignSelf: 'center', justifyContent: 'center', paddingLeft: 16, paddingRight: 8, borderWidth: 1.5, borderColor: colors.lightGrey}}>
              <TextInput
                ref={passwordInputRef}
                style={[text.label.medium, {flex: 1, width: '90%', height: '100%'}]}
                //autoFocus={true}
                autoCompleteType='password'
                secureTextEntry={!passwordVisible}
                textContentType='emailAddress'
                placeholder="••••••••"
                //clearButtonMode='while-editing'
                value={password}
                onChangeText={(password: string) => setPassword(password)}
                returnKeyType={'done'}
              />
              <Icon name={passwordVisible ? "eye-off-outline" : "eye-outline"} size={20} color={colors.lightGrey} onPress={() => setPasswordVisible(!passwordVisible)} />
            </View>
            {/* <Button label='Continue' onPress={() => login(email, password)} /* caption='Forgot your password?' *//> */}
          </View>
        </ScrollView>
        <Header 
          sheet
          left='back'
          {...{y}}
        />
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#BD2F39',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Login;