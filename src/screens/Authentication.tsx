import React, { useState, useRef } from 'react';
import { StyleSheet, Text, View , TextInput, Image, ScrollView} from 'react-native';

import { f, db, auth } from '../configs/firebase';
import { useNavigation } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import Animated, { useSharedValue } from 'react-native-reanimated';

// Components
import Header from '../components/Header';
import Wall from '../components/Wall';
import Login from './Login';
import Signup from './Signup';

// Design System
import text from '../design-system/text';
import colors from '../design-system/colors';
import dimensions from '../design-system/dimensions';
import { ActionBar, InputLabel, ListTitle } from '../design-system/interface';

const AuthStack = createStackNavigator();

const Authentication = () => {

  // navigation
  const { navigate, goBack } = useNavigation();

  // refs
  const fullnameInput = useRef<any>(null);
  const emailInput = useRef<any>(null);
  const passwordInput = useRef<any>(null);

  // state
  const [ fullname, setFullname ] = useState<any>();
  const [ email, setEmail ] = useState<any>();
  const [ password, setPassword ] = useState<any>();
  const [ viewHeight, setViewHeight ] = useState<any>(0);

  // variables
  const y = useSharedValue(0);

  // renders
  const CoverCopy = () => {
    return <View style={{minHeight: dimensions.HEADER_HEIGHT_AND_SAFE_AREA}} onLayout={(event) => { setViewHeight(Math.ceil(event.nativeEvent.layout.height))}}/>
  };

  const Index = ({navigation}:any) => {
    return(
      <View style={StyleSheet.absoluteFill}>
        {<Wall
        background
        initialPosition = {dimensions.HEIGHT * 1}
        animateOnMount={false}
        cover = {{
          format: null, // map || video || image || showcase
          url: false,
          //thumbnail: null,
          copyBlockHeight: null
        }}
        //feed={wall}
        details={<CoverCopy />}
        {...{y}}
      />}
        <Header 
          trailing='not-now'
          {...{y}}  
        />
      </View>
    );
  };

  return (
    <NavigationContainer independent >
      <AuthStack.Navigator headerMode="none">
        <AuthStack.Screen name="Index" component={Index} />
        <AuthStack.Screen name="Signup" component={Signup} />
        <AuthStack.Screen name="Login" component={Login} />
      </AuthStack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#BD2F39',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Authentication;