import React, { useState, useEffect, useMemo } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Toast from 'react-native-root-toast';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { useNavigation, useRoute } from '@react-navigation/native';
import Animated, { interpolateColor, interpolate, Extrapolate, useSharedValue, useAnimatedStyle } from 'react-native-reanimated';
import { StatusBar } from 'expo-status-bar';

import Header from '../components/Header';
import Wall from '../components/Wall';
import Vendor from './Vendor';

import colors from '../design-system/colors';
import text from '../design-system/text';
import dimensions from '../design-system/dimensions';
import { BottomGradient } from '../design-system/interface';

const VendorStack = createStackNavigator();

const Vendors = ({ ...props }) => {

    /* let toast = Toast.show('Request failed to send.', {
      duration: Toast.durations.LONG,
    }); */

    // navigation
    const route = useRoute();
    const { navigate } = useNavigation();

    // states
    const [ viewHeight, setViewHeight ] = useState<any>(0);

    // variables
    const y = useSharedValue(0);

    const CoverCopyBlock = () => {

        const animatedStyle = useAnimatedStyle(() => ({
          opacity: interpolate(
            y.value, 
            [0, 1],
            [1, 0],
            Extrapolate.CLAMP
          ),
        }));
    
        return (
          <Animated.View style={[{paddingLeft: 24, paddingRight: 48, paddingTop: 24, paddingBottom: 16}, animatedStyle]} onLayout={(event) => {setViewHeight(Math.ceil(event.nativeEvent.layout.height))}}>
            <Text style={[text.label.small, {marginBottom: 4}]}>New Feature</Text>
            <Text style={text.heading.xlarge}>PLAN YOUR WEEKENDS</Text>
            <Text style={text.paragraph.regular}>Add your favourite vendors to your bucket list and we’ll help you find them on site with our new tool.</Text>
          </Animated.View>
        );
    };

    const showcase = [
        {
          name: "100Ribs",
          video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv07025740000bs07knhjmb6p9ndct6f0.mp4?alt=media&token=da6b1577-d45f-46c3-a152-8cc0fcbf281a",
          image: "https://images.squarespace-cdn.com/content/v1/54c86088e4b059985165f545/1627969071039-OZZN4W81794TWY0QX2YV/626+new+vendor+template+%2887%29.png?format=750w",
          description: "Ribs, Chicken, Brisket, Fries, Lemonade",
          tags: "New Vendor",
          booth: "C74",
          category: "100 ribs to die for.",
          instagram: "https://www.instagram.com/100_ribs/",
          cta: "instagram",
          destination: "Vendor"
        },
        {
          name: "Be The Match",
          logo: "https://btm.azureedge.net/facebook.png",
          video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv07025740000bs07knhjmb6p9ndct6f0.mp4?alt=media&token=da6b1577-d45f-46c3-a152-8cc0fcbf281a",
          image: "https://secure2.convio.net/marrow/images/content/pagebuilder/BTM_MainDonationForm_Desktop_Image.jpg",
          description: "The power to cure blood cancer, sickle cell and other blood diseases is in the hands of ordinary people. Join the registry today.",
          tags: "626 Partner",
          booth: "C74",
          category: "626 Partner",
          website: "https://bethematch.org",
          destination: "Vendor"
        },
        {
          name: "Bunbao",
          video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv09044150000bt3iltget5mmaomtn8ng.mp4?alt=media&token=b5e0c0eb-fb12-4a2f-9e17-06f3c4d92575",
          image: "https://images.squarespace-cdn.com/content/v1/54c86088e4b059985165f545/1627683793481-QBTQUU6VHSINB5N1R9EN/64937766_1620584621418858_6576366383017523990_n.jpg?format=750w",
          description: "Shu Mai, Har Gow, Spicy Wonton, Shrimp & Scallop, BBQ Pork",
          booth: "C21",
          tags: "New Vendor",
          category: "We Deliver, We Cater!",
          website: "https://bunbao.com",
          destination: "Vendor"
          
        },
        {
          name: "Belly Bombz",
          video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv09044440000bqmj3cqnvlj9vtupprr0.mp4?alt=media&token=f5b2dfc5-b648-4a46-a2c8-fa1c1fd76175",
          image: "https://images.squarespace-cdn.com/content/v1/54c86088e4b059985165f545/1627353333894-Q9XEUABM3RVP0ZFF7UF3/IMG_6417.JPG?format=750w",
          description: "Korean Wings, Loaded Fries, Shrimp",
          booth: "C11",
          tags: "New Vendor",
          category: "Korean Inspired Kitchen",
          website: "https://bellybombz.com",
          destination: "Vendor"
        },
        {
          name: "Big & Long Potatoes",
          video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv09044a40000bpq9baiasdpsjvlo74s0.mp4?alt=media&token=3bdec746-d50d-47ee-afd3-35768a249c70",
          image: "https://images.squarespace-cdn.com/content/v1/54c86088e4b059985165f545/1628053414784-ZUA3U4CWP6O1G9S2UCGE/213831662_718774915509120_7161818734652792052_n.jpg?format=750w",
          description: "Swirl Potato",
          booth: "C71",
          tags: "New Vendor",
          category: "Famous potato swirls & Handcrafted artisan lemonades",
          instagram: "https://www.instagram.com/bigandlongpotatoswirl/",
          cta: "instagram",
          destination: "Vendor"
        }
    ];

    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: colors.pulp }]}>
          {<Wall
              background
              initialPosition = {dimensions.HEIGHT * 0}
              animateOnMount={true}
              cover = {{
              format: 'showcase', // map || video || image || showcase
              data: showcase,
              url: false,
              //thumbnail: null,
              copyBlockHeight: viewHeight
              }}
              //feed={wall}
              details={<CoverCopyBlock />}
              {...{y}}
          />}
          <Header
              theme='dark'
              //leading="search"
              center="Vendors"
              //trailing="account"
              {...{ y }}
          />
          <StatusBar style="light" />
      </View>
  );

}

export default Vendors;