import React, { useState, useRef } from 'react';
import { StyleSheet, Text, View , TextInput} from 'react-native';

import { f, db, auth } from '../configs/firebase';
import { useNavigation } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import Animated, { color, useSharedValue } from 'react-native-reanimated';

// Components
import Header from '../components/Header';
import { ScrollView } from 'react-native-gesture-handler';

// Design System
import text from '../design-system/text';
import colors from '../design-system/colors';
import dimensions from '../design-system/dimensions';
import { ActionBar, InputLabel, ListTitle, Icon } from '../design-system/interface';

const Signup = () => {

  // navigation
  const { navigate, goBack } = useNavigation();

  // refs
  const nameInputRef = useRef<any>(null);
  const emailInputRef = useRef<any>(null);
  const passwordInputRef = useRef<any>(null);

  // variables
  const y = useSharedValue(0);

  // state
  const [ name, setName ] = useState<any>();
  const [ email, setEmail ] = useState<any>();
  const [ password, setPassword ] = useState<any>();
  const [ passwordVisible, setPasswordVisible ] = useState<any>(false);

  const createNewProfile = (newUser:any) => {
    const appRef = db.collection("apps").doc("qQRrNtSfBbWkxJ71jwcr");
    appRef.collection("profiles").doc(newUser.uid).set({
      name: name,
      email: newUser.email
    })
    .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch((error) => {
        console.error("Error adding document: ", error);
    });
  };

  const signup = (email:any, password:any) => {
    if (email && password) {
      f.auth().createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // Signed in
        var user = userCredential.user;
        createNewProfile(user);
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
      })
    } else {
      alert("Please fill in missing information.")
    }
  };

  return (
  <View style={StyleSheet.absoluteFill}>
        <ScrollView
        //keyboardDismissMode='on-drag'
        keyboardShouldPersistTaps='never'
          >
          <View style={{top: dimensions.HEADER_HEIGHT + 38}}>
            <ListTitle title="Create your account"/>
            <View style={{height: 56, marginTop: 24, width: dimensions.WIDTH - 32, borderRadius: 16, alignSelf: 'center', justifyContent: 'center', paddingLeft: 16, paddingRight: 8, borderWidth: 1.5, borderColor: colors.lightGrey}}>
              <TextInput
                ref={nameInputRef}
                style={[text.label.medium, {height: '100%'}]}
                autoFocus={true}
                autoCompleteType='name'
                keyboardType='default'
                textContentType='name'
                placeholder="Name"
                clearButtonMode='while-editing'
                value={name}
                returnKeyType={'next'}
                onChangeText={(name: string) => setName(name)}
                onSubmitEditing={() => {emailInputRef.current.focus()}}
                blurOnSubmit={false}
              />
            </View>
            <View style={{height: 56, marginTop: 24, width: dimensions.WIDTH - 32, borderRadius: 16, alignSelf: 'center', justifyContent: 'center', paddingLeft: 16, paddingRight: 8, borderWidth: 1.5, borderColor: colors.lightGrey}}>
              <TextInput
                ref={emailInputRef}
                style={[text.label.medium, {height: '100%'}]}
                keyboardType='email-address'
                placeholder="Email"
                clearButtonMode='while-editing'
                value={email}
                returnKeyType={'next'}
                onChangeText={(email: string) => setEmail(email)}
                onSubmitEditing={() => {passwordInputRef.current.focus()}}
                blurOnSubmit={false}
              />
            </View>
            <View style={{flex: 1, height: 56, marginTop: 24, width: dimensions.WIDTH - 32, flexDirection: "row", borderRadius: 16, alignSelf: 'center', alignItems: 'center', marginBottom: 40, justifyContent: 'space-between', paddingLeft: 16, paddingRight: 8, borderWidth: 1.5, borderColor: colors.lightGrey}}>
              <TextInput
                ref={passwordInputRef}
                style={[text.label.medium, {flex: 1, width: '90%', height: '100%'}]}
                secureTextEntry={!passwordVisible}
                autoCompleteType='off'
                textContentType='newPassword'
                placeholder="Password"
                value={password}
                onChangeText={(password: string) => setPassword(password)}
                returnKeyType={'done'}
              />
              <Icon name={passwordVisible ? "eye-off-outline" : "eye-outline"} size={20} color={colors.lightGrey} onPress={() => setPasswordVisible(!passwordVisible)} />
            </View>
            {/* <ActionBar label='Continue' trailing="arrow-right" onPress={() => signup(email, password)}/> */}
          </View>
        </ScrollView>
        <Header 
          sheet
          leading='back'
          {...{y}}
        />
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#BD2F39',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Signup;