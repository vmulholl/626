import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { MotiView } from 'moti'
import { useNavigation, useRoute } from '@react-navigation/native';
import Animated, { useSharedValue } from 'react-native-reanimated';
import { StatusBar } from 'expo-status-bar';

import Header from '../components/Header';
import Wall from '../components/Wall';

import colors from '../design-system/colors';
import dimensions from '../design-system/dimensions';
import text from '../design-system/text';
import { BottomGradient, FAB, ActionBar, Label } from '../design-system/interface';
import { promptBrowser } from '../functions/functions';

const Event = ({...props}) => {

  // navigation
  const route = useRoute();
  const { navigate } = useNavigation();

  // states
  const [ viewHeight, setViewHeight ] = useState<any>(0);

  // variables
  const y = useSharedValue(0);

  // renders
  const CoverCopy = () => {
    return <View style={{minHeight: dimensions.HEADER_HEIGHT_AND_SAFE_AREA}} onLayout={(event) => { setViewHeight(Math.ceil(event.nativeEvent.layout.height))}}/>
  };

  const wall = [
    {
      title: "",
      data:[
        //event details
        {
          id: "a1",
          format: "event-details",
          title: route.params.title || "title",
          venue: route.params.venue || "venue",
          venue_address: route.params.venue_address || "venue_address",
          label: route.params.label || "label",
          date: route.params.date,
          start_month: route.params.start_month,
          start_day: route.params.start_day,
          end_day: route.params.end_day,
          media: route.params.media
        }
      ]
    }
  ]; 

  return (
    <View style={[StyleSheet.absoluteFill, {backgroundColor: colors.pulp, borderRadius: 40}]}>
      <StatusBar style="auto" />
      {<Wall
        background
        // TODO ->
        initialPosition = {dimensions.HEIGHT * 0.5 + dimensions.HEADER_HEIGHT_AND_SAFE_AREA} //because listheader component is empty but requires 103 of height
        animateOnMount={false}
        cover = {{
          format: 'stickers', // map || video || image || sticker || showcase
          image: route.params.image || false,
          //thumbnail: null,
          copyBlockHeight: viewHeight
        }}
        details={<CoverCopy />}
        feed={wall}
        footer
        {...{y}}
      />}
      <Header
        leading = "x"
        //trailing = "share"
        {...{y}}
      />
      {/* <ActionBar animate leading="disc" label="Tickets starting at 5$" trailing="arrow-right" onPress={() => promptBrowser('https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR2WTSQrHhxGqjYj8-KPBXLA055zJMj2uM-7VQ8l41IoQv0aggGD0eQKdPo')} /> */}
    </View>
  );
}

export default Event;