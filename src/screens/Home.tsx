import React, { useState, useEffect, useMemo } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Toast from 'react-native-root-toast';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { useNavigation, useRoute } from '@react-navigation/native';
import Animated, { useSharedValue, interpolate, Extrapolate, useAnimatedStyle } from 'react-native-reanimated';
import { setStatusBarHidden, StatusBar } from 'expo-status-bar';

import Header from '../components/Header';
import Wall from '../components/Wall';
import Event from './Event';

import colors from '../design-system/colors';
import text from '../design-system/text';
import dimensions from '../design-system/dimensions';
import { BottomGradient } from '../design-system/interface';

const HomeStack = createStackNavigator();

const Home = ({...props}) => {

  /* let toast = Toast.show('Request failed to send.', {
    duration: Toast.durations.LONG,
  }); */

  // navigation
  const route = useRoute();
  const { navigate } = useNavigation();

  // states
  const [ viewHeight, setViewHeight ] = useState<any>(0);

  // variables
  const y = useSharedValue(0);

  const CoverCopyBlock = () => {

    const animatedStyle = useAnimatedStyle(() => ({
      opacity: interpolate(
        y.value, 
        [0, 1],
        [1, 0],
        Extrapolate.CLAMP
      ),
    }));

    return (
      <Animated.View style={[{paddingLeft: 24, paddingRight: 48, paddingTop: 24, paddingBottom: 16}, animatedStyle]} onLayout={(event) => {setViewHeight(Math.ceil(event.nativeEvent.layout.height))}}>
        <Text style={[text.label.small, {marginBottom: 4}]}>Introducing our new app</Text>
        <Text style={text.heading.xlarge}>WELCOME HOME</Text>
        <Text style={text.paragraph.regular}>Discover new vendors and plan your next visit to the 626 Night Market.</Text>
      </Animated.View>
    );
  };

  const wall = [
    {
      title: "",
      data:[
        //tile-A
        {
          id: "a",
          format: "carousel",
          style: "small-portrait-card",
          title: "Upcoming events",
          more: false,
          thumbnails: [
              {
                id: "a1",
                title: "626 Night Market",
                venue: "Santa Anita Park",
                venue_address: "285 W Huntington Dr, Arcadia, CA 91007, USA",
                label: "626 Night Market: Santa Anita Park",
                date: "July 9 - 11",
                start_month: "JUL",
                start_day: "9",
                end_day: "11",
                image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
                destination: "Event"
              },
              {
                id: "a2",
                title: "626 Night Market",
                venue: "Santa Anita Park",
                venue_address: "285 W Huntington Dr, Arcadia, CA 91007, USA",
                label: "626 Night Market: Santa Anita Park",
                date: "July 16 - 18",
                start_month: "JUL",
                start_day: "16",
                end_day: "18",
                image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
                destination: "Event"
              },
              {
                id: "a3",
                title: "626 Night Market",
                venue: "Alameda Fairgrounds",
                venue_address: "Pleasanton, California 94566, USA",
                label: "626 Night Market: Alameda Fairgrounds",
                date: "August 6 - 8",
                start_month: "AUG",
                start_day: "6",
                end_day: "8",
                tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
                image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',                
                destination: "Event"
              },
              {
                id: "a4",
                title: "626 Night Market",
                venue: "Alameda Fairgrounds",
                venue_address: "Pleasanton, California 94566, USA",
                label: "626 Night Market: Alameda Fairgrounds",
                date: "August 20 - 22",
                start_month: "AUG",
                start_day: "20",
                end_day: "22",
                tickets_url: 'https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU',
                image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',                
                destination: "Event"
              },
              {
                id: "a5",
                title: "626 Night Market",
                venue: "Alameda Fairgrounds",
                venue_address: "Pleasanton, California 94566, USA",
                label: "626 Night Market: Alameda Fairgrounds",
                date: "August 27 - 29",
                start_month: "AUG",
                start_day: "27",
                end_day: "29",
                image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
                destination: "Event"
              },
              {
                id: "a6",
                title: "626 Night Market",
                venue: "Santa Anita Park",
                venue_address: "285 W Huntington Dr, Arcadia, CA 91007, USA",
                label: "626 Night Market: Santa Anita Park",
                date: "September 3 - 5",
                start_month: "SEP",
                start_day: "3",
                end_day: "5",
                location: "Santa Anita Park",
                image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
                destination: "Event"
              },
              {
                id: "a7",
                title: "626 Night Market",
                venue: "Alameda Fairgrounds",
                venue_address: "Pleasanton, California 94566, USA",
                label: "626 Night Market: Alameda Fairgrounds",
                date: "September 24 - 26",
                start_month: "SEP",
                start_day: "24",
                end_day: "26",
                image: 'https://scontent.fymy1-2.fna.fbcdn.net/v/t1.6435-9/198522897_4320511821312752_8468203304665697891_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=340051&_nc_ohc=3D2F02dWCZQAX9kYK3T&_nc_ht=scontent.fymy1-2.fna&oh=01132cad9dbededa2cd89d2130002f13&oe=612E3AF9',
                destination: "Event"
              }
          ]
        },
        //tile-A
        {
          id: "b",
          format: "carousel",
          style: "small-portrait-card",
          title: "New vendor alert!",
          more: false,
          thumbnails: [
            {
              name: "100Ribs",
              video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv07025740000bs07knhjmb6p9ndct6f0.mp4?alt=media&token=da6b1577-d45f-46c3-a152-8cc0fcbf281a",
              image: "https://images.squarespace-cdn.com/content/v1/54c86088e4b059985165f545/1627969071039-OZZN4W81794TWY0QX2YV/626+new+vendor+template+%2887%29.png?format=750w",
              description: "Ribs, Chicken, Brisket, Fries, Lemonade",
              tags: "New Vendor",
              booth: "C74",
              category: "100 ribs to die for.",
              instagram: "https://www.instagram.com/100_ribs/",
              cta: "instagram",
              destination: "Vendor"
            },
            {
              name: "Bunbao",
              video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv09044150000bt3iltget5mmaomtn8ng.mp4?alt=media&token=b5e0c0eb-fb12-4a2f-9e17-06f3c4d92575",
              image: "https://images.squarespace-cdn.com/content/v1/54c86088e4b059985165f545/1627683793481-QBTQUU6VHSINB5N1R9EN/64937766_1620584621418858_6576366383017523990_n.jpg?format=750w",
              description: "Shu Mai, Har Gow, Spicy Wonton, Shrimp & Scallop, BBQ Pork",
              booth: "C21",
              tags: "New Vendor",
              category: "We Deliver, We Cater!",
              website: "https://bunbao.com",
              destination: "Vendor"
            },
            {
              name: "Belly Bombz",
              video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv09044440000bqmj3cqnvlj9vtupprr0.mp4?alt=media&token=f5b2dfc5-b648-4a46-a2c8-fa1c1fd76175",
              image: "https://images.squarespace-cdn.com/content/v1/54c86088e4b059985165f545/1627353333894-Q9XEUABM3RVP0ZFF7UF3/IMG_6417.JPG?format=750w",
              description: "Korean Wings, Loaded Fries, Shrimp",
              booth: "C11",
              tags: "New Vendor",
              category: "Korean Inspired Kitchen",
              website: "https://bellybombz.com",
              destination: "Vendor"
            },
            {
              name: "Big & Long Potatoes",
              video: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv09044a40000bpq9baiasdpsjvlo74s0.mp4?alt=media&token=3bdec746-d50d-47ee-afd3-35768a249c70",
              image: "https://images.squarespace-cdn.com/content/v1/54c86088e4b059985165f545/1628053414784-ZUA3U4CWP6O1G9S2UCGE/213831662_718774915509120_7161818734652792052_n.jpg?format=750w",
              description: "Swirl Potato",
              booth: "C71",
              tags: "New Vendor",
              category: "Famous potato swirls & Handcrafted artisan lemonades",
              instagram: "https://www.instagram.com/bigandlongpotatoswirl/",
              cta: "instagram",
              destination: "Vendor"
            }
          ]
        }
      ]
    }
  ];

  useEffect(() => {
    setStatusBarHidden(false, 'fade')
  },[])

  return (
    <View style={[StyleSheet.absoluteFill, {backgroundColor: colors.pulp}]}>
      <StatusBar style="dark" />
      {<Wall
        background
        initialPosition = {dimensions.HEIGHT * 0.6}
        animateOnMount={true}
        cover = {{
          format: null, // map || video || image || showcase
          url: false,
          //thumbnail: null,
          copyBlockHeight: viewHeight
        }}
        details={<CoverCopyBlock />}
        feed={wall}
        footer
        {...{y}}
      />}
      <Header
        center = "logo"
        //trailing = "account"
        {...{y}}
      />
      {/* <FAB /> */}
    </View>
  );

  
}

export default Home;