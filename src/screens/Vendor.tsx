import React, { useState, useMemo, useCallback } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { MotiView } from 'moti'
import { useNavigation, useRoute } from '@react-navigation/native';
import Animated, { useSharedValue } from 'react-native-reanimated';
import { StatusBar } from 'expo-status-bar';

import Header from '../components/Header';
import Wall from '../components/Wall';

import colors from '../design-system/colors';
import dimensions from '../design-system/dimensions';
import text from '../design-system/text';
import { BottomGradient, FAB, ActionBar, Label } from '../design-system/interface';
import { promptBrowser } from '../functions/functions';
import { useRef } from 'react';

const Vendor = ({...props}) => {

  // refs
  const bottomSheetModalRef = useRef<any>(null);

  // navigation
  const route = useRoute();
  const { navigate } = useNavigation();

  // states
  const [ viewHeight, setViewHeight ] = useState<any>(0);

  // variables
  const y = useSharedValue(0);
  const snapPoints = useMemo(() => ['25%', '50%'], []);

  // callbacks
  const handlePresentModalPress = useCallback(() => {
    bottomSheetModalRef.current?.present();
  }, []);
  const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index);
  }, []);
  

  // renders
  const CoverCopy = () => {
    return <View style={{minHeight: dimensions.HEADER_HEIGHT_AND_SAFE_AREA}} onLayout={(event) => { setViewHeight(Math.ceil(event.nativeEvent.layout.height))}}/>
  };

  const wall = [
    {
      title: "",
      data:[
        //event details
        {
          id: "00",
          format: "brand-details",
          name: route.params.name,
          logo: route.params.logo,
          //media: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv09044440000bqmj3cqnvlj9vtupprr0.mp4?alt=media&token=f5b2dfc5-b648-4a46-a2c8-fa1c1fd76175",
          image: route.params.image,
          description: route.params.description,
          booth: route.params.booth,
          tags: route.params.tags,
          category: route.params.category,
          website: route.params.website,
          instagram: route.params.instagram,
          cta: route.params.cta
        }
      ]
    }
  ]; 

  return (
    <View style={[StyleSheet.absoluteFill, {backgroundColor: colors.pulp, borderRadius: 40}]}>
      <StatusBar style="auto" />
      {<Wall
        background
        // TODO ->
        initialPosition = {dimensions.HEIGHT * 0.5 + dimensions.HEADER_HEIGHT_AND_SAFE_AREA} //because listheader component is empty but requires 103 of height
        animateOnMount={true}
        cover = {{
          format: 'stickers', // map || video || image || sticker || showcase
          image: route.params.image,
          //thumbnail: null,
          copyBlockHeight: viewHeight
        }}
        feed={wall}
        details={<CoverCopy />}
        {...{y}}
      />}
      <Header
        leading = "x"
        //trailing = "share"
        {...{y}}
      />
      {/* <ActionBar animate leading="disc" label="Tickets starting at 5$" trailing="arrow-right" onPress={() => promptBrowser('https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR2WTSQrHhxGqjYj8-KPBXLA055zJMj2uM-7VQ8l41IoQv0aggGD0eQKdPo')} /> */}
    </View>
  );
};

export default Vendor;