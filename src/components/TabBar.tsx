import React from 'react';
import { View, Text, Pressable, StyleSheet } from 'react-native';

import { BlurView } from 'expo-blur';
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import { StackActions } from '@react-navigation/native';

import colors from '../design-system/colors';
import dimensions from '../design-system/dimensions';
import text from '../design-system/text';

const Icon = createIconSetFromIcoMoon(
  require('../../assets/fonts/selection.json'),
  'IcoMoon',
  'icomoon.ttf'
);

const TabBar = ({ state, descriptors, navigation }) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <>
    <View 
      style={{
        position: 'absolute', 
        zIndex: 0, 
        height: 56, 
        width: dimensions.WIDTH - 2 * 48, 
        bottom: 40, 
        borderRadius: 16, 
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        alignSelf: 'center',
        backgroundColor: 'white',
        opacity: 0.7,
        shadowColor: colors.accentColor,
        shadowOffset: {
          width: 0,
          height: 16,
        },
        shadowOpacity: 0.3,
        shadowRadius: 20, 
      }} 
    />
    <BlurView 
        intensity={100}
        style={{
            overflow: 'hidden',
            position: 'absolute', 
            zIndex: 1, 
            height: 56, 
            width: dimensions.WIDTH - 2 * 48, 
            bottom: 40, 
            borderRadius: 16, 
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            alignSelf: 'center',
            backgroundColor: 'transparent',
        }}
        >
          <View 
            pointerEvents='none'
            style={{
              position: 'absolute', 
              zIndex: 2,
              height: 56,
              width: dimensions.WIDTH - 2 * 48,
              alignItems: 'center',
              justifyContent: 'space-evenly',
              flexDirection: 'row',
              backgroundColor: 'transparent'
            }}
          >
            <View style={{height: 16, width: 2, borderRadius: 4, backgroundColor: '#DEDEDE'}} />
            <View style={{height: 16, width: 2, borderRadius: 4, backgroundColor: '#DEDEDE'}} />
            <View style={{height: 16, width: 2, borderRadius: 4, backgroundColor: '#DEDEDE'}} />
          </View>
        {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const icons = ['home', 'calendar', 'stall', 'settings'];

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <Pressable
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1, height: '100%', justifyContent: 'space-between', alignItems: 'center', alignContent: 'center', paddingHorizontal: 4, paddingTop: 11 }}
          >
            <Icon name={icons[index]} size={21} color={isFocused ? colors.blue : colors.black} />
            <Text style={[styles.label, { color: isFocused ? colors.blue : colors.black, bottom: 6 }]}>
              {label}
            </Text>
          </Pressable>
        );
      })}
    </BlurView>
    </>
  );
};

const styles = StyleSheet.create({
    label: {
        //fontFamily: "Rounded-Medium",
        //textTransform: 'uppercase',
        fontSize: 10,
        letterSpacing: 10 * 0.06,
        lineHeight: 10 * 1.2,
        color: colors.black
    },
});

export default TabBar;