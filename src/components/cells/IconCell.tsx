import React from 'react';
import { StyleSheet, Text, View, Pressable, Image } from 'react-native';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { Icon } from '../../design-system/interface';

const IconCell = ({...props}) => {

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            paddingVertical: 12,
            flexDirection: 'row',
            marginHorizontal: 16,
            //backgroundColor: 'yellow',
            alignItems: props.action || props.caption ? 'flex-start' : 'center',
            //justifyContent: 'center',
        },
        cell: {
            marginVertical: 8,
            flexDirection: 'row',
            //backgroundColor: 'blue'
        }
    });

    return (
        <Pressable onPress={props.onPress} style={styles.container}>
            { props.leading && <Icon name={props.icon} color={colors.black} onPress={props.onPress} /> }
            <View style={{flex: 1, paddingHorizontal: 8}}>
                <Text 
                    numberOfLines={props.action || props.caption ? 1 : 2} 
                    style={[text.label.regular, {maxWidth: '95%'}]}
                >
                    {props.title}
                </Text>
                {props.action && <Text style={[text.label.regular, {color: colors.blue, paddingTop: 4}]}>{props.action}</Text>}
                {props.caption && <Text style={[text.label.regular, {color: colors.darkGrey, paddingTop: 4}]}>{props.caption}</Text>}
            </View>
            { props.onPress && <Icon name={'chevron-right'} size={12} color={colors.darkGrey} onPress={props.onPress} /> }
        </Pressable>
    );
};

export default IconCell;