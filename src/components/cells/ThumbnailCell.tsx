import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';

import { useNavigation } from '@react-navigation/native';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';

const ThumbnailCell = ({...props}) => {

    // navigation
    const { navigate } = useNavigation();

    return (
        <Pressable onPress={() => navigate(props.destination, {...props})} style={styles.cell}>
            <View style={{overflow: 'hidden', height: 60, width: props.thumbnail == 'square' ? 60 : Math.floor((60 / 9) * 16), justifyContent: 'center', alignItems: 'center', borderRadius: props.thumbnail == 'square' ? 12 : 8}}>
                <Image
                    style={{position: 'absolute', zIndex: 0, height: '100%', width: '100%', backgroundColor: colors.softWhite}}
                    source={{ }}
                    resizeMode='cover'
                />
                <Text style={[text.label.small, {paddingTop: 4, textAlign: 'center'}]}>{props.start_month}</Text>
                <Text style={[text.label.large, {textAlign: 'center'}]}>{props.start_day}</Text>
            </View>
            <View style={{flex: 1, paddingLeft: 16, paddingRight: 24, justifyContent: 'center'}}>
                <Text numberOfLines={1} style={[text.label.small, {paddingBottom: 4, color: colors.darkGrey}]}>{props.eyebrow || props.date}</Text>
                <Text numberOfLines={2} style={text.label.regular}>{props.label || props.heading || "Title"}</Text>
            </View>
        </Pressable>
    );
}

const styles = StyleSheet.create({
    cell: {
        //maxHeight: 84,
        paddingVertical: 10,
        marginHorizontal: 16,
        flexDirection: 'row',
        borderBottomColor: colors.lightGrey,
        borderBottomWidth: 0.75,
        alignItems: 'flex-start',
        //backgroundColor: 'yellow'
    }
});

export default ThumbnailCell;