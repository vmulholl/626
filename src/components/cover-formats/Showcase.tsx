import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { useNavigation } from '@react-navigation/native';
import { Video, AVPlaybackStatus } from 'expo-av';
import Animated, { Extrapolate, interpolate, useAnimatedStyle } from 'react-native-reanimated';
import { LinearGradient } from 'expo-linear-gradient';

import FullScreenVideo from '../thumbnails/FullScreenVideo';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { ListTitle } from '../../design-system/interface';


const Showcase = ({...props}) => {

  // refs
  const videoRef = useRef<any>(null);

  return (
    <ScrollView 
      contentContainerStyle={[{backgroundColor: 'black'}]}
      showsVerticalScrollIndicator={false}
      decelerationRate={0}
      disableIntervalMomentum
      snapToInterval={dimensions.HEIGHT}
    >
    { 
        props.cover.data.map((item, index) => {
            return <FullScreenVideo key={index} {...item}/>
        })
    }

    </ScrollView>
  );
}

const styles = StyleSheet.create({
  card: {
    //alignSelf: 'center',
    height: Math.floor((dimensions.WIDTH - 2 * dimensions.MARGIN_16) * 1.15),
    width: dimensions.WIDTH - 2 * dimensions.MARGIN_16,
    borderRadius: 32,
    marginVertical: 8,
    justifyContent: 'flex-end',
    overflow: 'hidden',
    backgroundColor: colors.softWhite
  },
  textBlock: {
      padding: 24,
      //backgroundColor: 'yellow'
  }
});

export default Showcase;