import React, { useMemo, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { useNavigation } from '@react-navigation/native';
import { Video, AVPlaybackStatus } from 'expo-av';
import Animated, { Extrapolate, interpolate, useAnimatedStyle } from 'react-native-reanimated';
import { LinearGradient } from 'expo-linear-gradient';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { ListTitle } from '../../design-system/interface';


const Stickers = ({...props}) => {

  // variables
  const stickers = [
    {
      name: "Takoyaki Tanota",
      media: "https://firebasestorage.googleapis.com/v0/b/loop-41ede.appspot.com/o/video-roll%2Fv07025740000bs07knhjmb6p9ndct6f0.mp4?alt=media&token=da6b1577-d45f-46c3-a152-8cc0fcbf281a",
      description: "",
      tags: "New Vendor",
      category: "Food"
    }
  ];

  const animatedStickerStyle = useAnimatedStyle(() => ({
    /* top: interpolate(
      props.y.value, 
      [0.05, 1],
      [0, - (dimensions.HEIGHT / 2 - dimensions.HEADER_HEIGHT_AND_SAFE_AREA)],
      Extrapolate.CLAMP
    ), */
    transform: [
      {
      scaleX: interpolate(
        props.y.value, 
        [0.05, 1],
        [1, 0.9],
        Extrapolate.CLAMP
      )
      },
      {
      scaleY: interpolate(
        props.y.value, 
        [0.05, 1],
        [1, 0.9],
        Extrapolate.CLAMP
      )
      }
    ],
    opacity: interpolate(
        props.y.value, 
        [0, 1],
        [1, 0],
        Extrapolate.CLAMP
    ),
    shadowOpacity: interpolate(
      props.y.value, 
      [0, 0.05],
      [0.2, 0],
      Extrapolate.CLAMP
    ) 
  })
  );

  const containerStyle = useMemo(
    () => [
      {
        height: 214,
        width: 214,
        shadowColor: colors.accentColor,
          shadowOffset: {
            width: 0,
            height: 16,
          },
        shadowRadius: 22
      },
      animatedStickerStyle,
    ],
    [animatedStickerStyle]
  );

  return (
    <View style={[StyleSheet.absoluteFill, {marginTop: dimensions.HEADER_HEIGHT_AND_SAFE_AREA, height: dimensions.HEIGHT / 2 - dimensions.HEADER_HEIGHT_AND_SAFE_AREA, alignItems: 'center', justifyContent: 'center'}]}>
    <Animated.View style={containerStyle}>
        <Image
          style={{
            borderRadius: 40,
            //borderWidth: 12,
            //borderColor: colors.softWhite,
            height: "100%",
            width: "100%",
            backgroundColor: colors.softWhite
          }}
          source={{ uri: props.cover.image ? props.cover.image : props.image ? props.image : 'https://source.unsplash.com/random/382x500'}}
          resizeMode='cover'
        />
    </Animated.View>
    </View>
  );
};

export default Stickers;