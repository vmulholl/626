import React, { useMemo } from 'react';
import { StyleSheet, Pressable, View, Image } from 'react-native';

import Animated, { Extrapolate, interpolate, useAnimatedStyle } from 'react-native-reanimated';
import { LinearGradient } from 'expo-linear-gradient';

// Design System
import colors from '../design-system/colors';
import dimensions from '../design-system/dimensions';
import text from '../design-system/text';
import { BottomGradient } from '../design-system/interface';

const Background = ({...props}) => {

  // animation
  const animatedContainerStyle = useAnimatedStyle(() => ({
    opacity: interpolate(
      props.y.value, 
      [0, 1],
      [1, 0],
      Extrapolate.CLAMP
      )
  })
  );

  const animatedFramedThumbnailStyle = useAnimatedStyle(() => ({
    top: interpolate(
      props.y.value, 
      [0.05, 1],
      [dimensions.HEADER_HEIGHT_AND_SAFE_AREA + 24, - dimensions.HEADER_HEIGHT_AND_SAFE_AREA],
      Extrapolate.CLAMP
    ),
    transform: [
      {
      scaleX: interpolate(
        props.y.value, 
        [0, 0.05],
        [1, 0.9],
        Extrapolate.CLAMP
      )
      },
      {
      scaleY: interpolate(
        props.y.value, 
        [0, 0.05],
        [1, 0.9],
        Extrapolate.CLAMP
      )
      }
    ],
    shadowOpacity: interpolate(
      props.y.value, 
      [0, 0.05],
      [0.2, 0],
      Extrapolate.CLAMP
      ) 
  })
  );

  const containerStyle = useMemo(
    () => [
      {
        position: 'absolute',
        zIndex: 1,
        //top: 119 + 24,
        //backgroundColor: colors.blue
      },
      animatedContainerStyle,
    ],
    [animatedContainerStyle]
  );

  const framedThumbnailStyle = useMemo(
    () => [
      {
        height: 214,
        width: 214,
        shadowColor: colors.accentColor,
          shadowOffset: {
            width: 0,
            height: 16,
          },
        //shadowOpacity: 0.2,
        shadowRadius: 22
      },
      animatedFramedThumbnailStyle,
    ],
    [animatedFramedThumbnailStyle]
  );

  const fullFramedVideoStyle = useMemo(
    () => [
      {
        height: 844,
        width: dimensions.WIDTH,
        backgroundColor: 'yellow'
      },
      animatedContainerStyle,
    ],
    [animatedContainerStyle]
  );

  return (
    <View style={[StyleSheet.absoluteFill, {flex: 1, zIndex: -1, alignItems: 'center', backgroundColor: colors.green}]}>

      <Image
        style={{
          height: '100%',
          width: '100%',
          backgroundColor: colors.pulp
        }}
        source={require('../../assets/cover.png')}
        resizeMode='cover'
      />
    </View>
  );
};

export default Background;