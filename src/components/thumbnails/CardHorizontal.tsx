import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable, ScrollView } from 'react-native';

import { useNavigation } from '@react-navigation/native';
/* import { db, auth } from '../../../firebase'; */
import { db } from '../../configs/firebase';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { ListTitle } from '../../design-system/interface';

const CardHorizontal = ({...props}) => {

  // navigation
  const { navigate } = useNavigation();

  // states
  const [ linkData, setLinkData ] = useState(null) 

  // variables
  const appRef = db.collection("apps").doc("qQRrNtSfBbWkxJ71jwcr");

  /* useEffect(() => {
    const getLinkData = (referenceCollection, referenceId) => {
      appRef.collection(referenceCollection).doc(referenceId)
      .get()
      .then((doc) => {
          let data;
          if (doc.exists) {
              data = doc.data();
          } else {
              // doc.data() will be undefined in this case
              console.log("No such document!");
          }
          setLinkData(data);
      }).catch((error) => {
          console.log("Error getting document:", error);
      });
    };

    getLinkData(props.reference_collection, props.reference_id)
  }, []) */

  return (
    <Pressable
          style={styles.container}
          onPress={() => navigate(props.destination, {...props})}
      >
        <View style={styles.textBlock}>
            {!!props.eyebrow && <Text style={text.label.small}>{props.eyebrow}</Text>}
            {!!props.heading && <Text style={[text.label.small, {paddingBottom: 8}]}>{props.heading}</Text>}
            {!!props.paragraph && <Text style={text.label.small}>{props.paragraph}</Text>}
        </View>
        <Image
          style={styles.card}
          //source={props.media ? { uri: props.media } : require('../../../assets/wallpaper.png')}
          resizeMode='cover'
        />
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container:  {
    //backgroundColor: 'yellow'
  },
  card: {
    alignSelf: 'center',
    height: (dimensions.WIDTH - 16 * 2) / 16 * 9,
    width: dimensions.WIDTH - 16 * 2,
    borderRadius: 16,
    marginVertical: 8,
    justifyContent: 'flex-end',
    overflow: 'hidden',
    backgroundColor: colors.softWhite
  },
  textBlock: {
    paddingTop: 24,
    paddingBottom: 8, 
    paddingHorizontal: 8,
    //backgroundColor: 'orange'
  }
});

export default CardHorizontal;