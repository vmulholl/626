import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { useNavigation } from '@react-navigation/native';
import { Video, AVPlaybackStatus } from 'expo-av';
import Animated, { Extrapolate, interpolate, useAnimatedStyle } from 'react-native-reanimated';
import { LinearGradient } from 'expo-linear-gradient';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { BottomGradient } from '../../design-system/interface';


const FullScreenVideo = ({...props}) => {

  // refs
  const videoRef = useRef<any>(null);

  // navigation
  const { navigate } = useNavigation();

  return (
    <Pressable 
      style={{height: dimensions.HEIGHT, width: dimensions.WIDTH}}
      onPress={() => navigate(props.destination, {...props})}
      >
        <View style={[{position: 'absolute', zIndex: 3, bottom: 136, paddingLeft: 24, paddingRight: 48, paddingTop: 24, paddingBottom: 16}]}>
          <Text style={[text.label.small, {color: colors.softWhite, marginBottom: 4}]}>{props.tags}</Text>
          <Text numberOfLines={1} style={[text.heading.xlarge, {color: colors.softWhite}]}>{props.name}</Text>
          <Text style={[text.paragraph.regular, {color: colors.softWhite}]}>{props.description || "Add your favourite vendors to your bucket list and we’ll help you find them on site with our new tool."}</Text>
        </View>
        <Video
          ref={videoRef}
          style={{height: '100%', width: '100%'}}
          //source={require('../../../assets/video.mp4')}
          source={{
            uri: props.video,
          }}
          shouldPlay
          //useNativeControls
          resizeMode="cover"
          isLooping
          isMuted
          //onPlaybackStatusUpdate={status => setStatus(() => status)}
        />
        <View style={{position: 'absolute', bottom: 0, height: dimensions.HEIGHT * 0.5, width: '100%'}}>
          <BottomGradient rgb="000, 000, 000" />
        </View>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  
});

export default FullScreenVideo;