import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';

import { useNavigation } from '@react-navigation/native';
//import { db, auth } from '../../configs/firebase';
import { BlurView } from 'expo-blur';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { ListTitle } from '../../design-system/interface';

const SmallPortraitCard = ({...props}) => {

  // navigation
  const { navigate } = useNavigation();
  
  /* return (
    <Pressable
          style={styles.container}
          onPress={() => navigate(props.destination, {...props})}
      >
        <View>
        {
        props.destination == "Event" &&
        <View style={{position: 'absolute', zIndex: 1, height: 40, width: 40, right: 8, bottom: 16, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.softWhite, borderRadius: 10}}>
          <Text style={{fontSize: 11, fontWeight: '600'}}>{props.start_month}</Text>
          <Text style={{fontSize: 13, fontWeight: '600'}}>{props.start_day}</Text>
        </View>
        }
        <Image
          style={styles.card}
          source={props.media ? { uri: props.media } : require('../../../assets/wallpaper.png')}
          resizeMode='cover'
        />
        </View>
        <View style={styles.textBlock}>
            {!!props.label && <Text numberOfLines={2} style={[text.label.regular, {paddingBottom: 8}]}>{props.label}</Text>}
        </View>
    </Pressable>
  ); */

  return (
    <Pressable 
      style={styles.container}
      onPress={() => navigate(props.destination, {...props})}
    >
    <View style={styles.cardShadow}/>
    <BlurView
      intensity={100}
      style={styles.glassCard}
    >
      {
        props.destination == "Event" &&
        <View style={{position: 'absolute', zIndex: 1, height: 40, width: 40, right: 8, bottom: 8, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.softWhite, borderRadius: 10}}>
          <Text style={{fontSize: 11, fontWeight: '600'}}>{props.start_month}</Text>
          <Text style={{fontSize: 13, fontWeight: '600'}}>{props.start_day}</Text>
        </View>
        }
      <Image
        style={{height: '100%', width: '100%'}}
        source={props.image ? { uri: props.image } : require('../../../assets/wallpaper.png')}
        resizeMode='cover'
      />
    </BlurView>
    <View style={styles.textBlock}>
      {(props.label || props.name) && <Text numberOfLines={2} style={[text.label.regular]}>{props.label || props.name}</Text>}
    </View>
    </Pressable>
  );

};

const styles = StyleSheet.create({
  container: {
    marginVertical: 8,
    marginLeft: 8,
    //backgroundColor: 'yellow'
  },
  glassCard: {
    height: (((dimensions.WIDTH - 16) - 8 * 3) / 2) / 0.75,
    width: ((dimensions.WIDTH - 16) - 8 * 3) / 2,
    overflow: 'hidden',
    borderRadius: 16,
    backgroundColor: 'transparent'
  },
  cardShadow: {
    position: 'absolute',
    zIndex: 0,
    height: (((dimensions.WIDTH - 16) - 8 * 3) / 2) / 0.75,
    width: ((dimensions.WIDTH - 16) - 8 * 3) / 2,
    borderRadius: 16,
    backgroundColor: colors.softWhite,
    opacity: 0.3,
    shadowColor: colors.accentColor,
    shadowOffset: {
      width: 0,
      height: 16,
    },
    shadowOpacity: 0.3,
    shadowRadius: 20 
  },
  textBlock: {
    maxWidth: ((dimensions.WIDTH - 16) - 8 * 3) / 2,
    paddingTop: 8, 
    paddingHorizontal: 16,
    //backgroundColor: 'blue'
  }
});

export default SmallPortraitCard;