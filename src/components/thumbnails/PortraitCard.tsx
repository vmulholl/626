import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { db } from '../../configs/firebase';
import { BlurView } from 'expo-blur';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { ListTitle } from '../../design-system/interface';

const PortraitCard = ({...props}) => {

  // navigation
  const { navigate } = useNavigation();

  // states
  const [ linkData, setLinkData ] = useState(null) 

  // variables
  const appRef = db.collection("apps").doc("qQRrNtSfBbWkxJ71jwcr");

  /* useEffect(() => {
    const getLinkData = (referenceCollection, referenceId) => {
      appRef.collection(referenceCollection).doc(referenceId)
      .get()
      .then((doc) => {
          let data;
          if (doc.exists) {
              //console.log("Document data:", doc.data());
              data = doc.data();
          } else {
              // doc.data() will be undefined in this case
              console.log("No such document!");
          }
          setLinkData(data);
      }).catch((error) => {
          console.log("Error getting document:", error);
      });
    };

    getLinkData(props.reference_collection, props.reference_id)
  }, []) */

  return (
    <View style={styles.container}>
    <Pressable style={styles.cardShadow} />
    <BlurView
      intensity={100}
      style={styles.glassCard}
    />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 8,
  },
  glassCard: {
    height: Math.floor((dimensions.WIDTH - 2 * dimensions.MARGIN_16) * 1.15),
    width: dimensions.WIDTH - 2 * dimensions.MARGIN_16,
    overflow: 'hidden',
    borderRadius: 32,
    backgroundColor: 'transparent'
  },
  cardShadow: {
    position: 'absolute',
    zIndex: 0,
    height: Math.floor((dimensions.WIDTH - 2 * dimensions.MARGIN_16) * 1.15),
    width: dimensions.WIDTH - 2 * dimensions.MARGIN_16,
    borderRadius: 32,
    backgroundColor: colors.softWhite,
    opacity: 0.3,
    shadowColor: colors.accentColor,
    shadowOffset: {
      width: 0,
      height: 16,
    },
    shadowOpacity: 0.2,
    shadowRadius: 20 
  }
});

export default PortraitCard;