import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { db, auth } from '../../configs/firebase';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { ListTitle } from '../../design-system/interface';

const SmallLandscapeCard = ({...props}) => {

  // navigation
  const { navigate } = useNavigation();

  // states
  const [ linkData, setLinkData ] = useState(null) 

  // variables
  const appRef = db.collection("apps").doc("qQRrNtSfBbWkxJ71jwcr");
  
    /* useEffect(() => {
      const getLinkData = (referenceCollection, referenceId) => {
        appRef.collection(referenceCollection).doc(referenceId)
        .get()
        .then((doc) => {
            let data;
            if (doc.exists) {
                //console.log("Document data:", doc.data());
                data = doc.data();
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
            }
            setLinkData(data);
        }).catch((error) => {
            console.log("Error getting document:", error);
        });
      };
  
      !!props.links && getLinkData(props.reference_collection, props.reference_id);
    }, []) */
  
    return (
      <Pressable
            style={styles.container}
            onPress={() => navigate(props.destination, {...props})}
        >
          <View>
          {
          props.destination == "Event" &&
          <View style={{position: 'absolute', zIndex: 1, height: 40, width: 40, right: 8, bottom: 16, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.softWhite, borderRadius: 10}}>
            <Text style={{fontSize: 11, fontWeight: '600'}}>{props.start_month}</Text>
            <Text style={{fontSize: 13, fontWeight: '600'}}>{props.start_day}</Text>
          </View>
          }
          <Image
            style={styles.card}
            source={props.media ? { uri: props.media } : require('../../../assets/wallpaper.png')}
            resizeMode='cover'
          />
          </View>
          <View style={styles.textBlock}>
              {!!props.label && <Text numberOfLines={2} style={[text.label.regular, {paddingBottom: 8}]}>{props.label}</Text>}
          </View>
      </Pressable>
    );
  }
  
  const styles = StyleSheet.create({
    container:  {
      //backgroundColor: 'yellow'
    },
    card: {
      height: (((dimensions.WIDTH - 16) - 8 * 3) / 2) / 16 * 9,
      width: ((dimensions.WIDTH - 16) - 8 * 3) / 2,
      borderRadius: 12,
      marginLeft: 8,
      marginVertical: 8,
      overflow: 'hidden',
      backgroundColor: colors.softWhite
    },
    textBlock: {
      maxWidth: ((dimensions.WIDTH - 16) - 8 * 3) / 2,
      paddingBottom: 8, 
      paddingHorizontal: 16,
      //backgroundColor: 'blue'
    }
  });
  
  export default SmallLandscapeCard;