import React, { useState, useEffect, useMemo } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';

import { LinearGradient } from 'expo-linear-gradient';
import Animated, { Extrapolate, interpolate, useAnimatedStyle } from 'react-native-reanimated';
import { useNavigation, useRoute } from '@react-navigation/native';
import { auth } from '../configs/firebase';
import { BlurView } from 'expo-blur';
import { Feather } from '@expo/vector-icons';

// Design System
import colors from '../design-system/colors'; 
import dimensions from '../design-system/dimensions';
import { TopGradient, Key } from '../design-system/interface';
import text from '../design-system/text';
import { promptShare } from '../functions/functions';

const Header = ({...props}) => {

  // navigation
  const route = useRoute();
  const { navigate, goBack } = useNavigation();

  // states
  const [ currentUser, setCurrentUser ] = useState<any>(null);

  // authentication hook
  useEffect(() => {
    const user = auth.currentUser;
    if (user && !user.isAnonymous) {
      setCurrentUser(user);
    } else {
      
    }
    return () => { }
  }, []);

  // stylesheet
  const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        flexDirection: 'column-reverse',
        height: dimensions.HEADER_HEIGHT_AND_SAFE_AREA,
        width: '100%',
        //backgroundColor: colors.accentColor,
        //opacity: 0.8
    },

    center: {
      position: 'absolute',
      height: dimensions.HEADER_HEIGHT,
      maxWidth: 180,
      paddingHorizontal: 16,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      //backgroundColor: 'yellow'
    },

    title: {
      position: 'absolute',
      height: dimensions.HEADER_HEIGHT,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      //backgroundColor: 'gold'
    },

    navigationBar: {
      flexDirection: 'row',
      height: dimensions.HEADER_HEIGHT,
      width: dimensions.WIDTH,
      alignItems: 'center',
      //backgroundColor: 'orange'
    }
});

  // animation
  const animatedStyle = useAnimatedStyle(() => ({
    opacity: interpolate(
      props.y.value, 
      [0, 1],
      [0, 1],
      Extrapolate.CLAMP
    ),
  }));

  const headerBackgroundStyle = useMemo(
    () => [
      {
        position: 'absolute',
        zIndex: 0,
        height: dimensions.HEADER_HEIGHT_AND_SAFE_AREA,
        width: '100%',
        //backgroundColor: colors.blue,
        //borderBottomColor: colors.lightGrey,
        //borderBottomWidth: 1,
      },
      animatedStyle,
    ],
    [animatedStyle]
  );

  return (
    <View style={styles.container}>
      <Animated.View style={headerBackgroundStyle}>
        <TopGradient />
      </Animated.View>
      <View style={styles.navigationBar}>
        { props.leading == "back" && <Key leading icon={'arrow-left'} onPress={() => goBack()}/> }
        { props.leading == "back-bg" && <Key leading background icon={'arrow-left'} onPress={() => goBack()}/> }
        { props.leading == "x" && <Key leading background icon={'x'} onPress={() => goBack()}/> }
        { props.leading == "search" && <Key leading background icon={'search'} onPress={() => goBack()}/> }

        { props.trailing == "edit-profile" && <Key trailing label="Edit Profile" labelColor={props.labelColor || colors.blue} onPress={props.dismiss} /> }
        { props.trailing == "not-now" && <Key trailing label="Not Now" labelColor={colors.blue} onPress={() => goBack()}/> }
        { props.trailing == "share" && <Key trailing background icon={'share'} onPress={() => promptShare('https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR2WTSQrHhxGqjYj8-KPBXLA055zJMj2uM-7VQ8l41IoQv0aggGD0eQKdPo')} /> }
        { props.trailing == "account" && <Key trailing background icon={'user'} onPress={() => currentUser ? navigate("Account") : navigate("Authentication")} /> }
      </View>
      { props.center && (
        <Pressable 
          style={route.name === 'Home' || props.center === 'logo' ? styles.center : styles.title} 
          //onPress={() => (route.name === 'Home') || props.center === 'logo' ? navigate('Menu') : goBack()}
          hitSlop={24}
        >
          { route.name === 'Home' || props.center === 'logo' ? 
            <Image
              style={{height: 20, maxWidth: '100%'}}
              source={require('../../assets/logo.png')}
              resizeMode='contain'
            />
          :
            <Text style={[text.label.medium, {color: props.theme == 'dark' ? colors.softWhite : colors.black}]}>{props.center}</Text>
          }
        </Pressable>
      )
      }
      { props.sheet &&
        <View style={{height: 38, justifyContent: 'center'}}>
          <View style={{height: 6, width: 32, borderRadius: 4, alignSelf: 'center', backgroundColor: colors.softWhite}} />
        </View>
      }
    </View>
  );
};

export default Header;