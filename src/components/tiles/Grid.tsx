import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import SmallLandscapeCard from '../thumbnails/SmallLandscapeCard';
import SmallPortraitCard from '../thumbnails/SmallPortraitCard';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import { ListTitle } from '../../design-system/interface';
import { ScrollView } from 'react-native-gesture-handler';

const Grid = ({...props}) => {

    // navigation
    const { navigate } = useNavigation();

    // style
    const styles = StyleSheet.create({
        container: {
          width: dimensions.WIDTH,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.pulp
        }
    });

    return (
        <View style={styles.container}>
            <ListTitle title={props.title} onPress={() => navigate("Collection", {...props})} />
            <ScrollView
                contentContainerStyle={{paddingRight: 8}}
                horizontal
                showsHorizontalScrollIndicator={false}
            >
                {
                    props.thumbnails.length && props.thumbnails.map((item:any, index:any) => {
                        if (props.style == "small-card-horizontal") {
                        return <SmallLandscapeCard key={index} {...item} />
                        } else if (props.style == "small-card-vertical") {
                        return <SmallPortraitCard key={index} {...item} />
                        }
                    })
                }
            </ScrollView>
        </View>
    );
};

export default Grid;