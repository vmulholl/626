import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { useNavigation } from '@react-navigation/native';

import SmallLandscapeCard from '../thumbnails/SmallLandscapeCard';
import SmallPortraitCard from '../thumbnails/SmallPortraitCard';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import { ListTitle } from '../../design-system/interface';
//import { ScrollView } from 'react-native-gesture-handler';

const Carousel = ({...props}) => {

    // navigation
    const { navigate } = useNavigation();

    // style
    const styles = StyleSheet.create({
        container: {
          width: dimensions.WIDTH,
          justifyContent: 'center',
          alignItems: 'center',
          //backgroundColor: 'rgba(237,237,234,0.5)'
        }
    });

    return (
        <View style={styles.container}>
            <ListTitle title={props.title} more={props.more} onPress={() => navigate("Collection", {...props})} />
            <ScrollView
                contentContainerStyle={{paddingLeft: 4, paddingRight: 12}}
                horizontal
                showsHorizontalScrollIndicator={false}
                disableScrollViewPanResponder
                directionalLockEnabled
            >
                {
                    props.thumbnails.length && props.thumbnails.map((item:any, index:any) => {
                        if (props.style == "small-landscape-card") {
                        return <SmallLandscapeCard key={index} {...item} />
                        } else if (props.style == "small-portrait-card") {
                        return <SmallPortraitCard key={index} {...item} />
                        }
                    })
                }
            </ScrollView>
        </View>
    );
};

export default Carousel;