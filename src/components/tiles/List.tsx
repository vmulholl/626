import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import ThumbnailCell from '../cells/ThumbnailCell';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';
import { ListTitle } from '../../design-system/interface';

const List = ({...props}) => {

    // navigation
    const { navigate } = useNavigation();

    // style
    const styles = StyleSheet.create({
        container: {
          width: dimensions.WIDTH,
          justifyContent: 'center',
          alignItems: 'center',
          //backgroundColor: colors.vellum
        }
    });

    return (
        <View style={styles.container}>
        { props.title && <ListTitle title={props.title} onPress={() => navigate("Collection", {...props})} /> }
        {
        props.thumbnails.length && props.thumbnails.map((item:any, index:any) => {
        if (props.style == "square-thumbnail-cell") {
            return <ThumbnailCell key={index} thumbnail='square' {...item} />
            }
        })
        }
        </View>
    );
};

export default List;