import React, { useState, useMemo, useCallback, useRef } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { BottomSheetModal } from '@gorhom/bottom-sheet';

import Header from '../../Header';

// Design System
import text from '../../../design-system/text';
import colors from '../../../design-system/colors'; 
import dimensions from '../../../design-system/dimensions'; 
import { ActionBar, Key, Label } from '../../../design-system/interface';
import { promptBrowser } from '../../../functions/functions';

const BrandDetails = ({...props}) => {

  // refs
  const bottomSheetModalRef = useRef<any>(null);

  // variables
  const snapPoints = useMemo(() => ['25%', '50%'], []);

  // callbacks
  const handlePresentModalPress = useCallback(() => {
    bottomSheetModalRef.current?.present();
  }, []);
  const handleDismissModalPress = useCallback(() => {
    bottomSheetModalRef.current?.dismiss();
  }, []);
  const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index);
  }, []);

  return(
    <View style={{width: '100%'}}>
      <View style={{width: '100%', alignItems: 'center', paddingHorizontal: 20, paddingVertical: 24}}> 
        { props.logo ? (
            <Image
              style={{height: 56, width: '70%'}}
              source={{uri: props.logo}}
              resizeMode='cover'
            />
        
          ) : (
        
          <Text style={text.heading.medium}>{props.name}</Text>
        
        )}
        <Text style={text.paragraph.small}>{props.category}</Text>
      </View>
      <ActionBar 
          items={[
            {
              onPress: () => promptBrowser(props.cta == "instagram" ? props.instagram : props.website),
              icon: props.cta == "instagram" ? "instagram" : "home",
              iconColor: colors.softWhite,
              label: props.cta == "instagram" ? "Instagram" : "Visit website",
              labelColor: colors.softWhite,
              bgColor: colors.blue,
            },
            {
              //onPress: () => {handlePresentModalPress()},
              icon: "stall",
              //label: "Locate booth",
              label: "Booth " + props.booth,
              bgColor: colors.softWhite,
            }
          ]}
        />
      <Label label="About" />
      <View style={{width: '100%', paddingHorizontal: 24, alignItems: 'left'}}> 
        <Text style={[text.paragraph.regular, {textAlign: "left"}]}>{props.description}</Text>
        {/* <Text style={[text.paragraph.regular, {paddingTop: 12, paddingBottom: 24, color: colors.blue}]}>Spread the word</Text> */}
      </View>
      <BottomSheetModal
        ref={bottomSheetModalRef}
        index={1}
        snapPoints={snapPoints}
        handleComponent={() => null}
        onChange={handleSheetChanges}
      >
        <View style={{position: 'absolute', zIndex: 1, paddingHorizontal: 8, flexDirection: 'row-reverse', alignItems: 'center', height: dimensions.HEADER_HEIGHT, width: '100%'}}>
          <Key label="Cancel" onPress={() => handleDismissModalPress()}/>
        </View>
        <View style={styles.contentContainer}>
          <Text>Awesome 🎉</Text>
        </View>
      </BottomSheetModal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    justifyContent: 'center',
    backgroundColor: 'grey',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
  },
});

export default BrandDetails;