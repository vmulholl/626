import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, Text, View } from 'react-native';

// Design System
import text from '../../../design-system/text';
import colors from '../../../design-system/colors'; 
import dimensions from '../../../design-system/dimensions'; 
import { ActionBar, Label } from '../../../design-system/interface';
import { promptBrowser } from '../../../functions/functions';

const EventDetails = ({...props}) => {
    
  return(
        <View style={{}}>
          <View style={{width: '100%', alignItems: 'center', paddingHorizontal: 20, paddingVertical: 24}}> 
            <Text style={text.heading.medium}>{props.title}</Text>
            <Text style={text.paragraph.small}>{props.date + '  |  ' + props.venue}</Text>
          </View>
          <View style={{justifyContent: 'space-evenly', flexDirection: 'row'}}>
            <ActionBar 
              items={[
                {
                  toggle: true,
                  icon: "notgoing",
                  iconColor: colors.softWhite,
                  label: "Going",
                  labelColor: colors.softWhite,
                  bgColor: colors.blue,
                  toggledIcon: "going",
                  toggledIconColor: colors.green,
                  toggledBgColor: colors.softWhite,
                  toggledLabel: "Going",
                  toggledLabelColor: colors.green
                },
                {
                  onPress: () => promptBrowser('https://www.626nightmarket.com/tickets-bay-area?fbclid=IwAR3zmtMcSmHTosaFp76iTw6GswmTl5RyiIWGgoFDNXdDgPNOGAvVwu4RpsU'),
                  icon: "ticket",
                  label: "Find tickets",
                  bgColor: colors.softWhite,
                },
              ]}
            />
          </View>
          <Label label="About" />
          <View style={{width: '100%', paddingHorizontal: 24, alignItems: 'left'}}> 
            <Text style={[text.paragraph.regular, {textAlign: "left"}]}>The iconic Californian festival features 250+ food, merchandise, crafts, artists, games, live concerts, and entertainment attractions in an epic event that appeals to all ages.</Text>
            {/* <Text style={[text.paragraph.regular, {paddingTop: 12, paddingBottom: 0, color: colors.blue}]}>Spread the word</Text> */}
          </View>
          {/* <Label label="Good to know" />
          <View style={{width: '100%', paddingHorizontal: 24, alignItems: 'left'}}> 
            <Text numberOfLines={1} style={[text.paragraph.regular, {fontFamily: "Rounded-Medium", textAlign: "left"}]}>{props.venue}</Text>
            <Text numberOfLines={1} style={[text.paragraph.regular, {textAlign: "left"}]}>{props.venue_address}</Text>
            <Text numberOfLines={1} style={[text.paragraph.regular, {paddingTop: 12, paddingBottom: 0, color: colors.blue}]}>Get directions</Text>
          </View> */}
          <Label label="Venue" />
          <View style={{width: '100%', paddingHorizontal: 24, alignItems: 'left'}}> 
            <Text numberOfLines={1} style={[text.paragraph.regular, {fontWeight: '600', textAlign: "left"}]}>Santa Anita Park</Text>
            <Text numberOfLines={1} style={[text.paragraph.regular, {textAlign: "left"}]}>285 W Huntington Dr, Arcadia, CA 91007, USA</Text>
            {/* <Text numberOfLines={1} style={[text.paragraph.regular, {paddingTop: 12, paddingBottom: 24, color: colors.blue}]}>Get directions</Text> */}
          </View>
        </View>
    );
};

export default EventDetails;