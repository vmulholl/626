import React, { useState, useMemo, useCallback, useRef } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { auth } from '../../../configs/firebase';
import { BottomSheetModal } from '@gorhom/bottom-sheet';

// Design System
import text from '../../../design-system/text';
import colors from '../../../design-system/colors'; 
import dimensions from '../../../design-system/dimensions'; 
import { ActionBar, Key, Label } from '../../../design-system/interface';
import { promptBrowser } from '../../../functions/functions';
import IconCell from '../../cells/IconCell';

const user = auth.currentUser;

const AccountDetails = ({...props}) => {

  console.log(user?.isAnonymous);

  // refs
  const bottomSheetModalRef = useRef<any>(null);

  // variables
  const snapPoints = useMemo(() => ['25%', '50%'], []);

  // callbacks
  const handlePresentModalPress = useCallback(() => {
    bottomSheetModalRef.current?.present();
  }, []);
  const handleDismissModalPress = useCallback(() => {
    bottomSheetModalRef.current?.dismiss();
  }, []);
  const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index);
  }, []);

  return(
    <View style={{width: '100%'}}>
      {/* <View style={{width: '100%', alignItems: user?.isAnonymous ? 'flex-start' : 'center', paddingHorizontal: 24, paddingVertical: 24}}> 
        <Text style={text.heading.medium}>{user?.isAnonymous ? "Sign up" : user?.uid}</Text>
        <Text onPress={() => null} style={[text.label.regular, {paddingTop: 4, color: colors.blue}]}>{user?.isAnonymous ? "Already have an account? Sign in" : "Edit Profile"}</Text>
      </View> */}
      <Label label="About 626 APP" />
      <IconCell leading icon="document" title="Terms of Service" onPress={() => promptBrowser("https://626-night-markets.flycricket.io/terms.html")} />
      <IconCell leading icon="privacy" title="Privacy Policy" onPress={() => promptBrowser("https://626-night-markets.flycricket.io/privacy.html")}/>
      <IconCell leading icon="mail" title="Contact Us" onPress={() => promptBrowser("https://www.626nightmarket.com/contact")}/>
      <IconCell leading icon="profile" title="Version 0.1.2"/>
      {/* <IconCell leading icon="calendar" title="App Info" /> */}
      {/* <BottomSheetModal
        ref={bottomSheetModalRef}
        index={1}
        snapPoints={snapPoints}
        handleComponent={() => null}
        onChange={handleSheetChanges}
      >
        <View style={{position: 'absolute', zIndex: 1, paddingHorizontal: 8, flexDirection: 'row-reverse', alignItems: 'center', height: dimensions.HEADER_HEIGHT, width: '100%'}}>
          <Key label="Cancel" onPress={() => handleDismissModalPress()}/>
        </View>
        <View style={styles.contentContainer}>
          <Text>Awesome 🎉</Text>
        </View>
      </BottomSheetModal> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    justifyContent: 'center',
    backgroundColor: 'grey',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
  },
});

export default AccountDetails;