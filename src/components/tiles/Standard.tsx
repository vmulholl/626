import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';

import { useNavigation } from '@react-navigation/native';

//import CardHorizontal from '../thumbnails/CardHorizontal';
import PortraitCard from '../thumbnails/PortraitCard';

// Design System
import colors from '../../design-system/colors'; 
import dimensions from '../../design-system/dimensions'; 
import text from '../../design-system/text';

const Standard = ({...props}) => {

    // navigation
    const { navigate } = useNavigation();

    // style
    const styles = StyleSheet.create({
        container: {
          width: dimensions.WIDTH,
          justifyContent: 'center',
          alignItems: 'center',
          //backgroundColor: colors.pulp
        }
    });

    return (
        <View style={styles.container}>
        
        {   props.thumbnails &&
            props.thumbnails.map((thumbnail:any, index:any) => {
                return <PortraitCard key={index} {...thumbnail} />
                /* if (props.style == "card-horizontal") {
                return <CardHorizontal key={index} {...thumbnail} />
                } else {
                return <CardVertical key={index} {...thumbnail} />
                } */
            })
        }
        </View>
    );
};

export default Standard;