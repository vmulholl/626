import React, { useMemo, useRef } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';

import { Video, AVPlaybackStatus } from 'expo-av';
import Animated, { Extrapolate, interpolate, useAnimatedStyle } from 'react-native-reanimated';
import { LinearGradient } from 'expo-linear-gradient';

import Showcase from './cover-formats/Showcase';
import Stickers from './cover-formats/Stickers';

// Design System
import colors from '../design-system/colors';
import dimensions from '../design-system/dimensions';
import text from '../design-system/text';
import { BottomGradient } from '../design-system/interface';
import ProfilePicture from './cover-formats/ProfilePicture';

const Cover = ({...props}) => {

  // refs
  const videoRef = useRef<any>(null);

  // animation
  const animatedContainerStyle = useAnimatedStyle(() => ({
    opacity: interpolate(
      props.y.value, 
      [0, 1],
      [1, 0],
      Extrapolate.CLAMP
      )
  })
  );

  return (
    <View style={[StyleSheet.absoluteFill, {zIndex: 0}]}>
      {props.cover.format == "image" && 
      <Animated.View style={[{flex: 1}, animatedContainerStyle]}>
        <Image
          style={{
            height: '100%',
            width: '100%',
            backgroundColor: colors.pulp
          }}
          source={require('../../assets/photo.jpeg')}
          resizeMode='cover'
        />
        <View style={{position: 'absolute', bottom: props.initialPosition - props.cover.copyBlockHeight, height: dimensions.HEIGHT - props.initialPosition, width: '100%'}}>
          <BottomGradient rgb="221, 213, 201" />
        </View>
        <View style={{position: 'absolute', bottom: 0, height: props.initialPosition - props.cover.copyBlockHeight, width: '100%', backgroundColor: 'rgb(221, 213, 201)'}}/>
      </Animated.View>
      }
      {props.cover.format == "video" && 
      <Animated.View style={[{flex: 1}, animatedContainerStyle]}>
        <Image
          style={{
            height: '100%',
            width: '100%',
            backgroundColor: colors.pulp
          }}
          source={require('../../assets/photo.jpeg')}
          resizeMode='cover'
        />
        <View style={{position: 'absolute', bottom: props.initialPosition - props.cover.copyBlockHeight, height: dimensions.HEIGHT - props.initialPosition, width: '100%'}}>
          <BottomGradient rgb="221, 213, 201" />
        </View>
        <View style={{position: 'absolute', bottom: 0, height: props.initialPosition - props.cover.copyBlockHeight, width: '100%', backgroundColor: 'rgb(221, 213, 201)'}}/>
      </Animated.View>
      }
      {props.cover.format === "showcase" && <Showcase {...props} />}
      {props.cover.format === "stickers" && <Stickers {...props} />}
      {props.cover.format === "profile-picture" && <ProfilePicture {...props} />}
    </View>
  );
};

export default Cover;