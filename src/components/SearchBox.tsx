import React, { useState, useEffect, useMemo } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

// algolia
import PropTypes from 'prop-types';
import { connectSearchBox } from 'react-instantsearch-native';
import Toast from 'react-native-root-toast';
import { useNavigation, useRoute } from '@react-navigation/native';
import Animated, { useSharedValue } from 'react-native-reanimated';

import colors from '../design-system/colors';
import text from '../design-system/text';
import dimensions from '../design-system/dimensions';

const SearchBox = ({ currentRefinement, refine, ...props }) => {

    //Navigation
    const route = useRoute();
    const { navigate, goBack } = useNavigation();
    
    //State
    const inputAccessoryViewID = '123';
    const [ input, setInput ] = useState(/* (focus) ? focus.name : '' */);
  
    //Local Functions
    const _refine = (text: any) => {
      setInput(text);
      refine(text)
    };
  
    useEffect(() => {
      if (props.query != null) {
        setInput(props.query);
        refine(props.query)
      }
    }, [props.query])
  
    return(
      <>
      <View style={{position: 'absolute', zIndex: 1, marginTop: dimensions.HEADER_SAFE_PADDING, height: dimensions.HEADER_HEIGHT, alignSelf: 'center', width: '100%', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 16, backgroundColor: colors.pulp, borderBottomColor: colors.lightGrey, borderBottomWidth: 0.75}}>
          <View style={{flexDirection: 'row', height: 48, width: '100%', alignItems: 'center', borderRadius: 16, justifyContent: 'center',  overflow: 'hidden', paddingHorizontal: 8, backgroundColor: 'white'}}>
              <Icon name={'ios-chevron-back-sharp'} color={colors.black} onPress={() => goBack()} />            
              <TextInput
                  style={[text.input, { flex: 1, paddingLeft: 8}]}
                  onChangeText={text => _refine(text)}
                  value={(input) ? input : currentRefinement}
                  clearButtonMode='while-editing'
                  autoFocus
                  enablesReturnKeyAutomatically={true}
                  //inputAccessoryViewID={inputAccessoryViewID}
                  placeholder={props.placeholder}
                  placeholderTextColor={colors.darkGrey}
                  returnKeyType={'search'}
                  onSubmitEditing={() => console.log('Pick first Option')}
              /> 
          </View>
      </View>
      </>
    );
  };
  
  SearchBox.propTypes = {
    currentRefinement: PropTypes.string.isRequired,
    refine: PropTypes.func.isRequired,
  };
  
  export default connectSearchBox(SearchBox);
