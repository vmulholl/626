import React, { useCallback, useEffect, useRef, useMemo } from 'react';
import { StyleSheet, View, Text, Pressable, Image } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import BottomSheet, { BottomSheetSectionList } from '@gorhom/bottom-sheet';

import Backdrop from './Backdrop';
import Cover from './Cover';
import Standard from './tiles/Standard';
import List from './tiles/List';
import Carousel from './tiles/Carousel';
import Grid from './tiles/Grid';
import Background from './Background';
import EventDetails from './tiles/specific/EventDetails';
import BrandDetails from './tiles/specific/BrandDetails';
import AccountDetails from './tiles/specific/AccountDetails';

import dimensions from '../design-system/dimensions';
import { SectionTitle } from '../design-system/interface';
import { promptBrowser } from '../functions/functions';



const Wall = ({...props}) => {
  
    // hooks
    const sheetRef = useRef<BottomSheet>(null);
  
    // navigation
    const route = useRoute();
    const { navigate, goBack } = useNavigation();
    
    const snapPoints = useMemo(() => [props.initialPosition, dimensions.HEIGHT + props.cover.copyBlockHeight - dimensions.HEADER_HEIGHT_AND_SAFE_AREA], [props.cover.copyBlockHeight]);
    
    // render
    const renderItem = useCallback(
      ({ item }, index) => {
        if (item.format === 'event-details') {
          return <EventDetails key={index} {...item} />
        } else if (item.format === 'brand-details') {
          return <BrandDetails key={index} {...item} />
        } else if (item.format === 'editorial') {
          return <BrandDetails key={index} {...item} />
        } else if (item.format === 'account-details') {
          return <AccountDetails key={index} {...item} />
        } else if (item.format === 'standard') {
          return <Standard key={index} {...item} />
        } else if (item.format === 'list') {
          return <List key={index} {...item} />
        } else if (item.format === 'carousel') {
          return <Carousel key={index} {...item} />
        } else if (item.format === 'grid') {
          return <Grid key={index} {...item} />
        } else {
          return <Standard key={index} {...item} />
        }
      },[]
    );
  
    const renderSectionHeader = useCallback(
      ({ section }) => {
        return (
        !!section.title && <SectionTitle title={section.title} />
        )
      },
      []
    );

    const ListHeaderComponent = useCallback(
      () => {
        return (
          <View style={{width: '100%'}}>
            { props.details && props.details }
          </View>
        );
      },
      []
    );
  
    const CustomHandle = () => {
      return <View style={{height: dimensions.HEIGHT - props.initialPosition + 24, width: '100%'}}/>
    };
  
    const ListFooter = () => {
      return (
        <Pressable onPress={() => promptBrowser("https://www.626nightmarket.com/vendor-inquiry")} style={{width: dimensions.WIDTH, height: 425}}>
          <Image 
            style={{height: '100%', width: '100%'}}
            source={require('../../assets/vendorfooter.png')}
            resizeMode="cover"
          />
        </Pressable>
      )
    }
  
    return (
      <View style={[StyleSheet.absoluteFill, {height: dimensions.HEIGHT, bottom: -1}]}>
        { props.background && <Background {...props}/> }
        { props.cover.format && <Cover {...props}/> }
        <BottomSheet
          ref={sheetRef}
          snapPoints={snapPoints}
          //onChange={handleSheetChange}
          topInset={-(/* handle height -> */(dimensions.HEIGHT - props.initialPosition) + props.cover.copyBlockHeight)}
          handleComponent={() => <CustomHandle/> }
          backgroundComponent={() => null}
          backdropComponent={(BottomSheetBackdropProps) => <Backdrop {...BottomSheetBackdropProps}/>}
          animateOnMount={props.animatedOnMount}
          //animatedPosition={props.y}
          animatedIndex={props.y}
          enableOverDrag={false}
        >
          {/* @ts-ignore */}
          <BottomSheetSectionList
            sections={props.feed}
            contentContainerStyle={{}}
            renderSectionHeader={renderSectionHeader}
            stickySectionHeadersEnabled={false}
            ListHeaderComponent={() => <ListHeaderComponent/>}
            ListFooterComponent={props.footer && <ListFooter />}
            renderItem={renderItem}
            showsVerticalScrollIndicator={false}
            bounces={false}
          />
        </BottomSheet>
      </View>
    );
  };
  
  export default Wall;