import { Share, ActionSheetIOS, Platform } from 'react-native';
import * as WebBrowser from 'expo-web-browser';
import * as Linking from 'expo-linking';
import Clipboard from 'expo-clipboard';

export const promptCall = (phoneNumber:any) => {
    if (phoneNumber) {
        Linking.openURL(`tel:${phoneNumber}`)
    } else {
        alert('Email is not valid, please try again.')
    }
};

export const promptEmail = (emailAddress:any) => {
  if (emailAddress) {
      Linking.openURL(`mailto:${emailAddress}`)
  } else {
      alert('Email is not valid, please try again.')
  }
};

export const promptBrowser = async (url:any) => {
    await WebBrowser.openBrowserAsync(url);
};
  
export const copyToClipboard = (target:any) => {
    alert("Copied address to clipboard.");
    Clipboard.setString(target);
}; 
  
export const promptShare = async (target:any) => {
    try {
      const result = await Share.share({
        url: target,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
          console.log("Here are a 100 coins!");
        } else {
          // shared
          console.log("Here are a 100 coins!");
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
};